
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.ReportDAO;
import com.bh10.project.shorttermjobs.dto.ReportDto;
import com.bh10.project.shorttermjobs.entity.AdvertisementEntity;
import com.bh10.project.shorttermjobs.entity.ReportEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import com.bh10.project.shorttermjobs.exception.UserAlreadyReportedThisException;
import com.bh10.project.shorttermjobs.mapper.AdvertisementMapper;
import com.bh10.project.shorttermjobs.mapper.StjUserMapper;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ReportServiceTest {
    
    @Mock
    ReportDAO reportDAO;
    
    private ReportService underTest;
    
    public ReportServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        underTest = new ReportService(reportDAO);
    }
    
    @After
    public void tearDown() {
    }


    @Test(expected = UserAlreadyReportedThisException.class)
    public void testAddNewReport() throws Exception {
        List<ReportEntity> list = new ArrayList<>();
        ReportEntity entity = new ReportEntity();
        AdvertisementEntity advertisementEntity = new AdvertisementEntity();
        advertisementEntity.setId(1);
        StjUserEntity stjUserEntity = new StjUserEntity();
        stjUserEntity.setId(1);
        advertisementEntity.setUser(stjUserEntity);
        entity.setAdvertisement(advertisementEntity);
        entity.setReporterUser(stjUserEntity);
        list.add(entity);
        Mockito.when(reportDAO.reportListByUserId(ArgumentMatchers.anyInt())).thenReturn(list);
        underTest.addNewReport("", StjUserMapper.toDto(stjUserEntity), AdvertisementMapper.toDto(advertisementEntity));
    }


    @Test
    public void testDeleteReportById() throws Exception {
        underTest.deleteReportById(2);
        Mockito.verify(reportDAO, Mockito.times(1)).deleteReportById(ArgumentMatchers.anyInt());
    }


    @Test
    public void testFindReportById() throws Exception {
        ReportEntity entity = new ReportEntity();
        AdvertisementEntity advertisementEntity = new AdvertisementEntity();
        advertisementEntity.setId(1);
        StjUserEntity stjUserEntity = new StjUserEntity();
        stjUserEntity.setId(1);
        advertisementEntity.setUser(stjUserEntity);
        entity.setAdvertisement(advertisementEntity);
        entity.setReporterUser(stjUserEntity);
        Mockito.when(reportDAO.findReportById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.findReportById(2);
        Mockito.verify(reportDAO, Mockito.times(1)).findReportById(ArgumentMatchers.anyInt());
        assertEquals(ReportDto.class, underTest.findReportById(2).getClass());
    }


    @Test
    public void testReportList() throws Exception {
        underTest.reportList();
        Mockito.verify(reportDAO, Mockito.times(1)).reportList();
    }
    
}
