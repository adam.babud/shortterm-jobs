
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.EducationDAO;
import com.bh10.project.shorttermjobs.dao.ExperienceDAO;
import com.bh10.project.shorttermjobs.dao.HardSkillDAO;
import com.bh10.project.shorttermjobs.dao.JobApplicantDataDAO;
import com.bh10.project.shorttermjobs.dao.LanguageDAO;
import com.bh10.project.shorttermjobs.dao.SoftSkillDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.dto.JobApplicantDataDto;
import com.bh10.project.shorttermjobs.dto.StjUserDto;
import com.bh10.project.shorttermjobs.entity.EducationEntity;
import com.bh10.project.shorttermjobs.entity.ExperienceEntity;
import com.bh10.project.shorttermjobs.entity.GroupEntity;
import com.bh10.project.shorttermjobs.entity.HardSkillEntity;
import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import com.bh10.project.shorttermjobs.entity.LanguageEntity;
import com.bh10.project.shorttermjobs.entity.SoftSkillEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import com.bh10.project.shorttermjobs.entity.UserTypeEntity;
import java.security.Principal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProfileServiceTest {
    
    @Mock
    EducationDAO educationDAO;

    @Mock
    StjUserDAO stjUserDAO;

    @Mock
    ExperienceDAO experienceDAO;

    @Mock
    SoftSkillDAO softSkillDAO;

    @Mock
    HardSkillDAO hardSkillDAO;

    @Mock
    JobApplicantDataDAO jobApplicantDataDAO;

    @Mock
    LanguageDAO languageDAO;
    
    private ProfileService underTest;
    
    public ProfileServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        underTest = new ProfileService(educationDAO, stjUserDAO, experienceDAO, softSkillDAO, hardSkillDAO, jobApplicantDataDAO, languageDAO);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetUserTypeIdByPrincipal() throws Exception {
        Principal principal = () -> {
            return "email";
        };
        UserTypeEntity typeEntity = new UserTypeEntity();
        typeEntity.setId(2);
        StjUserEntity entity = new StjUserEntity();
        entity.setId(2);
        entity.setUserType(typeEntity);
        entity.setGroup(new GroupEntity());
        entity.setJobApplicantDataEntity(new JobApplicantDataEntity());
        Mockito.when(stjUserDAO.getUserByEmail("email")).thenReturn(entity);
        underTest.getUserTypeIdByPrincipal(principal);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getUserByEmail(ArgumentMatchers.any());
    }


    @Test
    public void testGetUserTypeIdByUserId() throws Exception {
        UserTypeEntity typeEntity = new UserTypeEntity();
        typeEntity.setId(2);
        StjUserEntity entity = new StjUserEntity();
        entity.setId(2);
        entity.setUserType(typeEntity);
        entity.setGroup(new GroupEntity());
        entity.setJobApplicantDataEntity(new JobApplicantDataEntity());
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.getUserTypeIdByUserId(2);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getStjUserById(ArgumentMatchers.anyInt());
    }


    @Test
    public void testFindUserByPrincipal() throws Exception {
        Principal principal = () -> {
            return "email";
        };
        UserTypeEntity typeEntity = new UserTypeEntity();
        typeEntity.setId(2);
        StjUserEntity entity = new StjUserEntity();
        entity.setId(2);
        entity.setUserType(typeEntity);
        entity.setGroup(new GroupEntity());
        entity.setJobApplicantDataEntity(new JobApplicantDataEntity());
        Mockito.when(stjUserDAO.getUserByEmail("email")).thenReturn(entity);
        underTest.findUserByPrincipal(principal);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getUserByEmail(ArgumentMatchers.any());
        Assert.assertEquals(StjUserDto.class, underTest.findUserByPrincipal(principal).getClass());
    }


    @Test
    public void testFindUserByEmail() throws Exception {
        StjUserEntity entity = new StjUserEntity();
        entity.setEmail("email");
        Mockito.when(stjUserDAO.getUserByEmail("email")).thenReturn(entity);
        underTest.findUserByEmail("email");
        Mockito.verify(stjUserDAO, Mockito.times(1)).getUserByEmail(ArgumentMatchers.any());
        Assert.assertEquals("email", underTest.findUserByEmail("email").getEmail());
        Assert.assertEquals(StjUserDto.class, underTest.findUserByEmail("email").getClass());
    }


    @Test
    public void testFindUserById() throws Exception {
        UserTypeEntity typeEntity = new UserTypeEntity();
        typeEntity.setId(2);
        StjUserEntity entity = new StjUserEntity();
        entity.setId(2);
        entity.setUserType(typeEntity);
        entity.setGroup(new GroupEntity());
        entity.setJobApplicantDataEntity(new JobApplicantDataEntity());
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.findUserById(2);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getStjUserById(ArgumentMatchers.anyInt());
        Assert.assertEquals(StjUserDto.class, underTest.findUserById(2).getClass());
    }


    @Test
    public void testFindEducationByUserId() throws Exception {
        UserTypeEntity typeEntity = new UserTypeEntity();
        typeEntity.setId(2);
        StjUserEntity entity = new StjUserEntity();
        entity.setId(2);
        entity.setUserType(typeEntity);
        entity.setGroup(new GroupEntity());
        entity.setJobApplicantDataEntity(new JobApplicantDataEntity());
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(entity);
        JobApplicantDataEntity jobApplicantDataEntity = new JobApplicantDataEntity();
        jobApplicantDataEntity.setUserId(entity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(entity)).thenReturn(jobApplicantDataEntity);
        List<EducationEntity> educationEntitys = new ArrayList<>();
        EducationEntity educationEntity = new EducationEntity();
        educationEntity.setJobApplicantData(jobApplicantDataEntity);
        EducationEntity educationEntity1 = new EducationEntity();
        educationEntity.setJobApplicantData(jobApplicantDataEntity);
        EducationEntity educationEntity2 = new EducationEntity();
        educationEntity.setJobApplicantData(jobApplicantDataEntity);
        educationEntitys.add(educationEntity);
        educationEntitys.add(educationEntity1);
        educationEntitys.add(educationEntity2);
        Mockito.when(educationDAO.findEducationByJobApplicant(jobApplicantDataEntity)).thenReturn(educationEntitys);
        underTest.findEducationByUserId(2);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getStjUserById(ArgumentMatchers.anyInt());
        Mockito.verify(jobApplicantDataDAO, Mockito.times(1)).findDataByUser(ArgumentMatchers.any());
        Mockito.verify(educationDAO, Mockito.times(1)).findEducationByJobApplicant(ArgumentMatchers.any());
        Assert.assertEquals(3, underTest.findEducationByUserId(2).size());
    }



    @Test
    public void testFindExperienceByUserId() throws Exception {
        UserTypeEntity typeEntity = new UserTypeEntity();
        typeEntity.setId(2);
        StjUserEntity entity = new StjUserEntity();
        entity.setId(2);
        entity.setUserType(typeEntity);
        entity.setGroup(new GroupEntity());
        entity.setJobApplicantDataEntity(new JobApplicantDataEntity());
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(entity);
        JobApplicantDataEntity jobApplicantDataEntity = new JobApplicantDataEntity();
        jobApplicantDataEntity.setUserId(entity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(entity)).thenReturn(jobApplicantDataEntity);
        List<ExperienceEntity> experienceEntitys = new ArrayList<>();
        ExperienceEntity experienceEntity = new ExperienceEntity();
        experienceEntity.setJobApplicantData(jobApplicantDataEntity);
        ExperienceEntity experienceEntity1 = new ExperienceEntity();
        experienceEntity1.setJobApplicantData(jobApplicantDataEntity);
        ExperienceEntity experienceEntity2 = new ExperienceEntity();
        experienceEntity2.setJobApplicantData(jobApplicantDataEntity);
        experienceEntitys.add(experienceEntity);
        experienceEntitys.add(experienceEntity1);
        experienceEntitys.add(experienceEntity2);
        Mockito.when(experienceDAO.findExperienceByJobApplicant(jobApplicantDataEntity)).thenReturn(experienceEntitys);
        underTest.findExperienceByUserId(2);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getStjUserById(ArgumentMatchers.anyInt());
        Mockito.verify(jobApplicantDataDAO, Mockito.times(1)).findDataByUser(ArgumentMatchers.any());
        Mockito.verify(experienceDAO, Mockito.times(1)).findExperienceByJobApplicant(ArgumentMatchers.any());
        Assert.assertEquals(3, underTest.findExperienceByUserId(2).size());
    }



    @Test
    public void testFindSofSkillById() throws Exception {
        UserTypeEntity typeEntity = new UserTypeEntity();
        typeEntity.setId(2);
        StjUserEntity entity = new StjUserEntity();
        entity.setId(2);
        entity.setUserType(typeEntity);
        entity.setGroup(new GroupEntity());
        entity.setJobApplicantDataEntity(new JobApplicantDataEntity());
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(entity);
        JobApplicantDataEntity jobApplicantDataEntity = new JobApplicantDataEntity();
        jobApplicantDataEntity.setUserId(entity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(entity)).thenReturn(jobApplicantDataEntity);
        List<SoftSkillEntity> softSkillEntitys = new ArrayList<>();
        SoftSkillEntity softSkillEntity = new SoftSkillEntity();
        softSkillEntity.setJobApplicantData(jobApplicantDataEntity);
        SoftSkillEntity softSkillEntity1 = new SoftSkillEntity();
        softSkillEntity1.setJobApplicantData(jobApplicantDataEntity);
        SoftSkillEntity softSkillEntity2 = new SoftSkillEntity();
        softSkillEntity2.setJobApplicantData(jobApplicantDataEntity);
        softSkillEntitys.add(softSkillEntity);
        softSkillEntitys.add(softSkillEntity1);
        softSkillEntitys.add(softSkillEntity2);
        Mockito.when(softSkillDAO.findSoftSkillByJobApplicant(jobApplicantDataEntity)).thenReturn(softSkillEntitys);
        underTest.findSofSkillById(2);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getStjUserById(ArgumentMatchers.anyInt());
        Mockito.verify(jobApplicantDataDAO, Mockito.times(1)).findDataByUser(ArgumentMatchers.any());
        Mockito.verify(softSkillDAO, Mockito.times(1)).findSoftSkillByJobApplicant(ArgumentMatchers.any());
        Assert.assertEquals(3, underTest.findSofSkillById(2).size());
    }


    @Test
    public void testFindHardSkillByUserId() throws Exception {
        UserTypeEntity typeEntity = new UserTypeEntity();
        typeEntity.setId(2);
        StjUserEntity entity = new StjUserEntity();
        entity.setId(2);
        entity.setUserType(typeEntity);
        entity.setGroup(new GroupEntity());
        entity.setJobApplicantDataEntity(new JobApplicantDataEntity());
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(entity);
        JobApplicantDataEntity jobApplicantDataEntity = new JobApplicantDataEntity();
        jobApplicantDataEntity.setUserId(entity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(entity)).thenReturn(jobApplicantDataEntity);
        List<HardSkillEntity> hardSkillEntitys = new ArrayList<>();
        HardSkillEntity hardSkillEntity = new HardSkillEntity();
        hardSkillEntity.setJobApplicantData(jobApplicantDataEntity);
        HardSkillEntity hardSkillEntity1 = new HardSkillEntity();
        hardSkillEntity1.setJobApplicantData(jobApplicantDataEntity);
        HardSkillEntity hardSkillEntity2 = new HardSkillEntity();
        hardSkillEntity2.setJobApplicantData(jobApplicantDataEntity);
        hardSkillEntitys.add(hardSkillEntity);
        hardSkillEntitys.add(hardSkillEntity1);
        hardSkillEntitys.add(hardSkillEntity2);
        Mockito.when(hardSkillDAO.findHardSkillByJobApplicant(jobApplicantDataEntity)).thenReturn(hardSkillEntitys);
        underTest.findHardSkillByUserId(2);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getStjUserById(ArgumentMatchers.anyInt());
        Mockito.verify(jobApplicantDataDAO, Mockito.times(1)).findDataByUser(ArgumentMatchers.any());
        Mockito.verify(hardSkillDAO, Mockito.times(1)).findHardSkillByJobApplicant(ArgumentMatchers.any());
        Assert.assertEquals(3, underTest.findHardSkillByUserId(2).size());
    }

 
    @Test
    public void testFindLanguageByUserId() throws Exception {
        UserTypeEntity typeEntity = new UserTypeEntity();
        typeEntity.setId(2);
        StjUserEntity entity = new StjUserEntity();
        entity.setId(2);
        entity.setUserType(typeEntity);
        entity.setGroup(new GroupEntity());
        entity.setJobApplicantDataEntity(new JobApplicantDataEntity());
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(entity);
        JobApplicantDataEntity jobApplicantDataEntity = new JobApplicantDataEntity();
        jobApplicantDataEntity.setUserId(entity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(entity)).thenReturn(jobApplicantDataEntity);
        List<LanguageEntity> languageEntitys = new ArrayList<>();
        LanguageEntity languageEntity = new LanguageEntity();
        languageEntity.setJobApplicantData(jobApplicantDataEntity);
        LanguageEntity languageEntity1 = new LanguageEntity();
        languageEntity1.setJobApplicantData(jobApplicantDataEntity);
        LanguageEntity languageEntity2 = new LanguageEntity();
        languageEntity2.setJobApplicantData(jobApplicantDataEntity);
        languageEntitys.add(languageEntity);
        languageEntitys.add(languageEntity1);
        languageEntitys.add(languageEntity2);
        Mockito.when(languageDAO.findLanguageByJobApplicant(jobApplicantDataEntity)).thenReturn(languageEntitys);
        underTest.findLanguageByUserId(2);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getStjUserById(ArgumentMatchers.anyInt());
        Mockito.verify(jobApplicantDataDAO, Mockito.times(1)).findDataByUser(ArgumentMatchers.any());
        Mockito.verify(languageDAO, Mockito.times(1)).findLanguageByJobApplicant(ArgumentMatchers.any());
        Assert.assertEquals(3, underTest.findLanguageByUserId(2).size());
    }


    @Test
    public void testFindDataByUserId() throws Exception {
        StjUserEntity stjUserEntity = new StjUserEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        applicantDataEntity.setUserId(stjUserEntity);
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.any())).thenReturn(stjUserEntity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(stjUserEntity)).thenReturn(applicantDataEntity);
        underTest.findDataByUserId(2);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getStjUserById(ArgumentMatchers.anyInt());
        Mockito.verify(jobApplicantDataDAO, Mockito.times(1)).findDataByUser(ArgumentMatchers.any());
        Assert.assertEquals(JobApplicantDataDto.class, underTest.findDataByUserId(2).getClass());
    }

    @Test
    public void testUpdatePersonalDatas() throws Exception {
        StjUserEntity stjUserEntity = new StjUserEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        applicantDataEntity.setUserId(stjUserEntity);
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.any())).thenReturn(stjUserEntity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(stjUserEntity)).thenReturn(applicantDataEntity);
        underTest.updatePersonalDatas(1, "address", "phoneNumber", "cvEmail", "cvName");
        Mockito.verify(stjUserDAO, Mockito.times(1)).getStjUserById(ArgumentMatchers.anyInt());
        Mockito.verify(jobApplicantDataDAO, Mockito.times(1)).findDataByUser(ArgumentMatchers.any());
        Mockito.verify(jobApplicantDataDAO, Mockito.times(1)).updatePersonalDatas(applicantDataEntity);
    }


    @Test
    public void testUpdateHardSkillById() throws Exception {
        HardSkillEntity entity = new HardSkillEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        entity.setJobApplicantData(applicantDataEntity);
        Mockito.when(hardSkillDAO.findHardSkillById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.updateHardSkillById(2, "school", 2);
        Mockito.verify(hardSkillDAO, Mockito.times(1)).updateHardSkill(ArgumentMatchers.any());
    }

    @Test
    public void testCreateNewHardSkillByJobApplicant() throws Exception {
        StjUserEntity stjUserEntity = new StjUserEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        stjUserEntity.setJobApplicantDataEntity(applicantDataEntity);
        applicantDataEntity.setUserId(stjUserEntity);
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(stjUserEntity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(stjUserEntity)).thenReturn(applicantDataEntity);
        underTest.createNewHardSkillByJobApplicant(2, "school", 2);
        Mockito.verify(hardSkillDAO, Mockito.times(1)).addNewHardSkill(ArgumentMatchers.any());
    }


    @Test
    public void testUpdateSoftSkillById() throws Exception {
        SoftSkillEntity entity = new SoftSkillEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        entity.setJobApplicantData(applicantDataEntity);
        Mockito.when(softSkillDAO.findSoftSkillById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.updateSoftSkillById(2, "school", 2);
        Mockito.verify(softSkillDAO, Mockito.times(1)).updateSoftSkill(ArgumentMatchers.any());
    }


    @Test
    public void testCreateNewSoftSkillByJobApplicant() throws Exception {
        StjUserEntity stjUserEntity = new StjUserEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        stjUserEntity.setJobApplicantDataEntity(applicantDataEntity);
        applicantDataEntity.setUserId(stjUserEntity);
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(stjUserEntity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(stjUserEntity)).thenReturn(applicantDataEntity);
        underTest.createNewSoftSkillByJobApplicant(2, "school", 2);
        Mockito.verify(softSkillDAO, Mockito.times(1)).addNewSoftSkill(ArgumentMatchers.any());
    }

    @Test
    public void testUpdateLanguageById() throws Exception {
        LanguageEntity entity = new LanguageEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        entity.setJobApplicantData(applicantDataEntity);
        Mockito.when(languageDAO.findLanguageById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.updateLanguageById(2, "school", "");
        Mockito.verify(languageDAO, Mockito.times(1)).updateLanguage(ArgumentMatchers.any());
    }


    @Test
    public void testCreateNewLanguageByJobApplicant() throws Exception {
        StjUserEntity stjUserEntity = new StjUserEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        stjUserEntity.setJobApplicantDataEntity(applicantDataEntity);
        applicantDataEntity.setUserId(stjUserEntity);
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(stjUserEntity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(stjUserEntity)).thenReturn(applicantDataEntity);
        underTest.createNewLanguageByJobApplicant(2, "school", "");
        Mockito.verify(languageDAO, Mockito.times(1)).addNewLanguage(ArgumentMatchers.any());
    }


    @Test
    public void testUpdateExperienceById() throws Exception {
        ExperienceEntity entity = new ExperienceEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        entity.setJobApplicantData(applicantDataEntity);
        Mockito.when(experienceDAO.findExperienceById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.updateExperienceById(2, "school", "", "", "qualification");
        Mockito.verify(experienceDAO, Mockito.times(1)).updateExperience(ArgumentMatchers.any());
    }


    @Test
    public void testCreateNewExperience() throws Exception {
        StjUserEntity stjUserEntity = new StjUserEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        stjUserEntity.setJobApplicantDataEntity(applicantDataEntity);
        applicantDataEntity.setUserId(stjUserEntity);
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(stjUserEntity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(stjUserEntity)).thenReturn(applicantDataEntity);
        underTest.createNewExperience(2, "school", "", "", "qualification");
        Mockito.verify(experienceDAO, Mockito.times(1)).addNewExperience(ArgumentMatchers.any());
    }


    @Test
    public void testUpdateEducationById() throws Exception {
        EducationEntity entity = new EducationEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        entity.setJobApplicantData(applicantDataEntity);
        Mockito.when(educationDAO.findEducationById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.updateEducationById(2, "school", new Date(1L), new Date(1L), "qualification");
        Mockito.verify(educationDAO, Mockito.times(1)).updateEducation(ArgumentMatchers.any());
    }


    @Test
    public void testCreateNewEducation() throws Exception {
        StjUserEntity stjUserEntity = new StjUserEntity();
        JobApplicantDataEntity applicantDataEntity = new JobApplicantDataEntity();
        stjUserEntity.setJobApplicantDataEntity(applicantDataEntity);
        applicantDataEntity.setUserId(stjUserEntity);
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(stjUserEntity);
        Mockito.when(jobApplicantDataDAO.findDataByUser(stjUserEntity)).thenReturn(applicantDataEntity);
        underTest.createNewEducation(2, "school", new Date(1L), new Date(1L), "qualification");
        Mockito.verify(educationDAO, Mockito.times(1)).addNewEducation(ArgumentMatchers.any());
    }


    @Test
    public void testDeleteHardSkill() throws Exception {
        HardSkillEntity entity = new HardSkillEntity();
        entity.setJobApplicantData(new JobApplicantDataEntity());
        Mockito.when(hardSkillDAO.findHardSkillById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.deleteHardSkill(28);
        Mockito.verify(hardSkillDAO, Mockito.times(1)).deleteHardSKill(entity);
    }

    @Test
    public void testDeleteSoftSkill() throws Exception {
        SoftSkillEntity entity = new SoftSkillEntity();
        entity.setJobApplicantData(new JobApplicantDataEntity());
        Mockito.when(softSkillDAO.findSoftSkillById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.deleteSoftSkill(10);
        Mockito.verify(softSkillDAO, Mockito.times(1)).deleteSoftSkill(entity);
    }


    @Test
    public void testDeleteLanguage() throws Exception {
        LanguageEntity entity = new LanguageEntity();
        entity.setJobApplicantData(new JobApplicantDataEntity());
        Mockito.when(languageDAO.findLanguageById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.deleteLanguage(2);
        Mockito.verify(languageDAO, Mockito.times(1)).deleteLanguage(entity);
    }

    @Test
    public void testDeleteExperience() throws Exception {
        ExperienceEntity entity = new ExperienceEntity();
        entity.setJobApplicantData(new JobApplicantDataEntity());
        Mockito.when(experienceDAO.findExperienceById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.deleteExperience(2);
        Mockito.verify(experienceDAO, Mockito.times(1)).deleteExperience(entity);
    }


    @Test
    public void testDeleteEducation() throws Exception {
        EducationEntity entity = new EducationEntity();
        entity.setJobApplicantData(new JobApplicantDataEntity());
        Mockito.when(educationDAO.findEducationById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.deleteEducation(2);
        Mockito.verify(educationDAO, Mockito.times(1)).deleteEducation(entity);
    }
    
}
