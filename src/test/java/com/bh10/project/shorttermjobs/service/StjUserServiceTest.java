package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.EmployerDataDAO;
import com.bh10.project.shorttermjobs.dao.JobApplicantDataDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import com.bh10.project.shorttermjobs.entity.UserTypeEntity;
import com.bh10.project.shorttermjobs.exception.EmailIsUsedException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StjUserServiceTest {

    private StjUserService stjUserService;

    @Mock
    StjUserDAO stjUserDAO;

    @Mock
    GroupService groupService;

    @Mock
    UserTypeService userTypeService;

    @Mock
    JobApplicantDataDAO jobApplicantDataDAO;

    @Mock
    EmployerDataDAO employerDataDAO;

    public StjUserServiceTest() {

    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        stjUserService = new StjUserService(stjUserDAO, groupService, userTypeService,
                jobApplicantDataDAO, employerDataDAO);
    }

    @After
    public void tearDown() {
    }

    @Test(expected = EmailIsUsedException.class)
    public void testIfEmailAlreadyExistCreateNewUser() throws Exception {
        Mockito.when(stjUserDAO.getCountOfUsersByEmail("com")).thenReturn(1L);
        stjUserService.createNewUser("com", "testbela", "1234", null);
    }

    @Test
    public void testCreateNewUserUserIsPrivate() throws Exception {
        UserTypeEntity userTypeEntity = new UserTypeEntity();
        userTypeEntity.setId(2);
        Mockito.when(stjUserDAO.getCountOfUsersByEmail("com")).thenReturn(0L);
        stjUserService.createNewUser("com", "testbela", "1234", userTypeEntity);
        Mockito.verify(jobApplicantDataDAO, Mockito.times(1)).persistData(ArgumentMatchers.any());
        Mockito.verify(employerDataDAO, Mockito.times(0)).createData(ArgumentMatchers.any());
    }

    @Test
    public void testCreateNewUserUserIsCompany() throws Exception {
        UserTypeEntity userTypeEntity = new UserTypeEntity();
        userTypeEntity.setId(3);
        Mockito.when(stjUserDAO.getCountOfUsersByEmail("com")).thenReturn(0L);
        stjUserService.createNewUser("com", "testbela", "1234", userTypeEntity);
        Mockito.verify(jobApplicantDataDAO, Mockito.times(0)).persistData(ArgumentMatchers.any());
        Mockito.verify(employerDataDAO, Mockito.times(1)).createData(ArgumentMatchers.any());

    }

    @Test(expected = EmailIsUsedException.class)
    public void testCreateNewAdmin() throws Exception {
        Mockito.when(stjUserDAO.getCountOfUsersByEmail("com")).thenReturn(1L);
        stjUserService.createNewAdmin("com", "testbela", "1234");
    }

    @Test
    public void testCreateNewAdminIsDataPersist() throws Exception {
        Mockito.when(stjUserDAO.getCountOfUsersByEmail("com")).thenReturn(0L);
        stjUserService.createNewAdmin("com", "testbela", "1234");
        Mockito.verify(stjUserDAO, Mockito.times(1)).createNewUser(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void testGetUserByEmail() throws Exception {
        StjUserEntity entity = new StjUserEntity();
        entity.setEmail("email");
        Mockito.when(stjUserDAO.getUserByEmail("email")).thenReturn(entity);
        stjUserService.getUserByEmail("email");
        Mockito.verify(stjUserDAO, Mockito.times(1)).getUserByEmail(ArgumentMatchers.any());
        Assert.assertEquals("email", stjUserService.getUserByEmail("email").getEmail());
    }

    @Test
    public void testLoadDeafultPrivateUserData() throws Exception {
        UserTypeEntity userTypeEntity = new UserTypeEntity();
        userTypeEntity.setId(2);
        Mockito.when(stjUserDAO.getCountOfUsersByEmail("com")).thenReturn(0L);
        stjUserService.createNewUser("com", "testbela", "1234", userTypeEntity);
        Mockito.verify(jobApplicantDataDAO, Mockito.times(1)).persistData(ArgumentMatchers.any());
        Mockito.verify(employerDataDAO, Mockito.times(0)).createData(ArgumentMatchers.any());
    }

    @Test
    public void testLoadDefaultEmployerData() throws Exception {
        UserTypeEntity userTypeEntity = new UserTypeEntity();
        userTypeEntity.setId(3);
        Mockito.when(stjUserDAO.getCountOfUsersByEmail("com")).thenReturn(0L);
        stjUserService.createNewUser("com", "testbela", "1234", userTypeEntity);
        Mockito.verify(jobApplicantDataDAO, Mockito.times(0)).persistData(ArgumentMatchers.any());
        Mockito.verify(employerDataDAO, Mockito.times(1)).createData(ArgumentMatchers.any());
    }

}
