
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.AdvertisementDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.dto.ApplicationDto;
import com.bh10.project.shorttermjobs.dto.StjUserDto;
import com.bh10.project.shorttermjobs.entity.AdvertisementEntity;
import com.bh10.project.shorttermjobs.entity.ApplicationEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import com.bh10.project.shorttermjobs.mapper.AdvertisementMapper;
import com.bh10.project.shorttermjobs.mapper.ApplicationMapper;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class AdvertisementServiceTest {
    
    @Mock
    AdvertisementDAO advertisementDAO;
    
    @Mock
    ApplicationService applicationService;

    @Mock
    StjUserDAO stjUserDAO;
    
    private AdvertisementService underTest;
    
    public AdvertisementServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        underTest = new AdvertisementService(advertisementDAO, applicationService, stjUserDAO);
    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void testFindAdvertiserByAdvertisement() throws Exception {
        List<StjUserDto> stjUserDtos = new ArrayList<>();
        StjUserDto dto = new StjUserDto();
        StjUserDto dto1 = new StjUserDto();
        stjUserDtos.add(dto);
        stjUserDtos.add(dto1);
        List<AdvertisementDto> advertisementDtos = new ArrayList<>();
        AdvertisementDto ad = new AdvertisementDto();
        AdvertisementDto ad1 = new AdvertisementDto();
        ad.setStjUserDto(dto);
        ad1.setStjUserDto(dto1);
        advertisementDtos.add(ad);
        advertisementDtos.add(ad1);
        Assert.assertEquals(stjUserDtos, underTest.findAdvertiserByAdvertisement(advertisementDtos));
   }

    @Test
    public void testDeleteAdvertisementById() throws Exception {
        underTest.deleteAdvertisementById(ArgumentMatchers.any());
        Mockito.verify(advertisementDAO, Mockito.times(1)).deleteAdvertisementById(ArgumentMatchers.any());
    }


    @Test
    public void testFindAllAdvertisement() throws Exception {
        AdvertisementEntity entity = new AdvertisementEntity();
        AdvertisementEntity entity1 = new AdvertisementEntity();
        AdvertisementEntity entity2 = new AdvertisementEntity();
        StjUserEntity stjUserEntity = new StjUserEntity();
        entity.setUser(stjUserEntity);
        entity1.setUser(stjUserEntity);
        entity2.setUser(stjUserEntity);
        List<AdvertisementEntity> advertisementEntitys = new ArrayList<>();
        advertisementEntitys.add(entity);
        advertisementEntitys.add(entity1);
        advertisementEntitys.add(entity2);
        Mockito.when(advertisementDAO.findAllAdvertisement()).thenReturn(advertisementEntitys);
        Assert.assertEquals(3, underTest.findAllAdvertisement().size());
        List<AdvertisementDto> dtos = new ArrayList<>();
        advertisementEntitys.forEach((p) -> {
            dtos.add(AdvertisementMapper.toDto(p));
        });
        Assert.assertEquals(dtos, underTest.findAllAdvertisement());
    }


    @Test
    public void testGetAllAdvertisementByJobTypeAndLocation() throws Exception {
        AdvertisementEntity entity = new AdvertisementEntity();
        AdvertisementEntity entity1 = new AdvertisementEntity();
        AdvertisementEntity entity2 = new AdvertisementEntity();
        StjUserEntity stjUserEntity = new StjUserEntity();
        entity.setUser(stjUserEntity);
        entity1.setUser(stjUserEntity);
        entity2.setUser(stjUserEntity);
        List<AdvertisementEntity> advertisementEntitys = new ArrayList<>();
        advertisementEntitys.add(entity);
        advertisementEntitys.add(entity1);
        advertisementEntitys.add(entity2);
        Mockito.when(advertisementDAO.findAdvertisementByJobTypeAndLocation(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(advertisementEntitys);
        Assert.assertEquals(3, underTest.getAllAdvertisementByJobTypeAndLocation(ArgumentMatchers.anyString(), ArgumentMatchers.anyString()).size());
        List<AdvertisementDto> dtos = new ArrayList<>();
        advertisementEntitys.forEach((p) -> {
            dtos.add(AdvertisementMapper.toDto(p));
        });
        Assert.assertEquals(dtos, underTest.getAllAdvertisementByJobTypeAndLocation(ArgumentMatchers.anyString(), ArgumentMatchers.anyString()));
    }


    @Test
    public void testGetCategorySearch() throws Exception {
        AdvertisementEntity entity = new AdvertisementEntity();
        AdvertisementEntity entity1 = new AdvertisementEntity();
        AdvertisementEntity entity2 = new AdvertisementEntity();
        StjUserEntity stjUserEntity = new StjUserEntity();
        entity.setUser(stjUserEntity);
        entity1.setUser(stjUserEntity);
        entity2.setUser(stjUserEntity);
        entity.setCategory("IT");
        entity1.setCategory("IT");
        entity2.setCategory("Law");
        List<AdvertisementEntity> advertisementEntitys = new ArrayList<>();
        advertisementEntitys.add(entity);
        advertisementEntitys.add(entity1);
        advertisementEntitys.add(entity2);
        Mockito.when(advertisementDAO.findAdvertisementByCategory(ArgumentMatchers.anyString())).thenReturn(advertisementEntitys);
        Assert.assertEquals(advertisementEntitys.size(), underTest.getCategorySearch(ArgumentMatchers.anyString()).size());
        List<AdvertisementDto> list = new ArrayList<>();
        advertisementEntitys.forEach((p) -> {
            list.add(AdvertisementMapper.toDto(p));
        });
        Assert.assertEquals(list, underTest.getCategorySearch(ArgumentMatchers.anyString()));
    }

    @Test
    public void testGetCategoriesSet() throws Exception {
        List<AdvertisementDto> list = new ArrayList<>();
        AdvertisementDto advertisementDto = new AdvertisementDto();
        advertisementDto.setCategory("IT");
        AdvertisementDto advertisementDto1 = new AdvertisementDto();
        advertisementDto1.setCategory("Building Industry");
        AdvertisementDto advertisementDto2 = new AdvertisementDto();
        advertisementDto2.setCategory("IT");
        
        list.add(advertisementDto);
        list.add(advertisementDto1);
        list.add(advertisementDto2);
        
        Assert.assertEquals(2, underTest.getCategoriesSet(list).size());
    }

    @Test
    public void testGetLocationSet() throws Exception {
        List<AdvertisementDto> list = new ArrayList<>();
        AdvertisementDto advertisementDto = new AdvertisementDto();
        advertisementDto.setLocation("Budapest");
        AdvertisementDto advertisementDto1 = new AdvertisementDto();
        advertisementDto1.setLocation("Debrecen");
        AdvertisementDto advertisementDto2 = new AdvertisementDto();
        advertisementDto2.setLocation("Budapest");
        
        list.add(advertisementDto);
        list.add(advertisementDto1);
        list.add(advertisementDto2);
        
        Assert.assertEquals(2, underTest.getLocationSet(list).size());
    }


    @Test
    public void testFindAdvertisementById() throws Exception {
        AdvertisementEntity entity = new AdvertisementEntity();
        StjUserEntity stjUserEntity = new StjUserEntity();
        entity.setUser(stjUserEntity);
        Mockito.when(advertisementDAO.findAdvertisementById(ArgumentMatchers.any())).thenReturn(entity);
        underTest.findAdvertisementById(ArgumentMatchers.any());
        Mockito.verify(advertisementDAO, Mockito.times(1)).findAdvertisementById(ArgumentMatchers.any());
    }
    
    @Test
    public void testCreateAdvertisement() throws Exception {
        StjUserEntity entity = new StjUserEntity();
        entity.setId(10);
        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(entity);
        underTest.createAdvertisement(1, "IT", "Budapest", new Date(2019-12-12), "blabla", "Law");
        Mockito.verify(advertisementDAO, Mockito.times(1)).createAdvertisement(ArgumentMatchers.any());
    }
    @Test
    public void testGetApplicantByAdvertisementId() throws Exception {
        AdvertisementEntity entity = new AdvertisementEntity();
        StjUserEntity stjUserEntity = new StjUserEntity();
        entity.setUser(stjUserEntity);
        List<ApplicationDto> list = new ArrayList<>();
        ApplicationEntity entity1 = new ApplicationEntity();
        entity1.setUser(stjUserEntity);
        entity1.setAdvertisement(entity);
        ApplicationEntity entity2 = new ApplicationEntity();
        entity2.setAdvertisement(entity);
        entity2.setUser(stjUserEntity);
        list.add(ApplicationMapper.toDto(entity1));
        list.add(ApplicationMapper.toDto(entity2));
        Mockito.when(applicationService.findAdvertisementById(ArgumentMatchers.any())).thenReturn(entity);
        Mockito.when(applicationService.findApplicantByAdvertisement(ArgumentMatchers.any())).thenReturn(list);
        Assert.assertEquals(2, underTest.getApplicantByAdvertisementId(2).size());
        Assert.assertEquals(list, underTest.getApplicantByAdvertisementId(2));
    }
   
}
