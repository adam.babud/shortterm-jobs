
package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.HardSkillDto;
import com.bh10.project.shorttermjobs.entity.HardSkillEntity;

public final class HardSkillMapper {

    public static HardSkillDto toDto(HardSkillEntity entity) {
        HardSkillDto dto = new HardSkillDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setLevel(entity.getLevel());
        return dto;
    }
}
