
package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.entity.AdvertisementEntity;

public final class AdvertisementMapper {
    
    public static AdvertisementDto toDto(AdvertisementEntity entity) {
        AdvertisementDto dto = new AdvertisementDto();
        dto.setId(entity.getId());
        dto.setDate(entity.getDate());
        dto.setWage(entity.getWage());
        dto.setLocation(entity.getLocation());
        dto.setJobType(entity.getJobType());
        dto.setDescription(entity.getDescription());
        dto.setStjUserDto(StjUserMapper.toDto(entity.getUser()));
        dto.setCategory(entity.getCategory());
        return dto;
    }
    
    public static AdvertisementEntity toEntity(AdvertisementDto dto) {
        AdvertisementEntity entity = new AdvertisementEntity();
        entity.setDate(dto.getDate());
        entity.setDescription(dto.getDescription());
        entity.setId(dto.getId());
        entity.setJobType(dto.getJobType());
        entity.setLocation(dto.getLocation());
        entity.setWage(dto.getWage());
        entity.setUser(StjUserMapper.toEntity(dto.getStjUserDto()));
        entity.setCategory(dto.getCategory());
        return entity;        
    }
}
