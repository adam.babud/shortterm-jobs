
package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.SoftSkillDto;
import com.bh10.project.shorttermjobs.entity.SoftSkillEntity;

public final class SoftSkillMapper {

    public static SoftSkillDto toDto(SoftSkillEntity entity) {
        SoftSkillDto dto = new SoftSkillDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setLevel(entity.getLevel());
        return dto;
    }
}
