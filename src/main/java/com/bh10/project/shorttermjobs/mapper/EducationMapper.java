
package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.EducationDto;
import com.bh10.project.shorttermjobs.entity.EducationEntity;

public final class EducationMapper {

        public static EducationDto toDto(EducationEntity entity) {
        EducationDto dto = new EducationDto();
        dto.setId(entity.getId());
        dto.setStartDate(entity.getStartDate());
        dto.setEndDate(entity.getEndDate());
        dto.setSchool(entity.getSchool());
        dto.setQualification(entity.getQualification());
        return dto;
    }
}
