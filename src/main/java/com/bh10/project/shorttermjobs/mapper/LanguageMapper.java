
package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.LanguageDto;
import com.bh10.project.shorttermjobs.entity.LanguageEntity;

public final class LanguageMapper {

    public static LanguageDto toDto(LanguageEntity entity) {
        LanguageDto dto = new LanguageDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setLevel(entity.getLevel());
        return dto;
    }
}
