
package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.ApplicationDto;
import com.bh10.project.shorttermjobs.entity.ApplicationEntity;

public final class ApplicationMapper {
    
     public static ApplicationDto toDto(ApplicationEntity entity) {
        ApplicationDto dto = new ApplicationDto();
        dto.setId(entity.getId());
        dto.setAdvertisement(AdvertisementMapper.toDto(entity.getAdvertisement()));
        dto.setUser(StjUserMapper.toDto(entity.getUser()));
        return dto;
    }
}
