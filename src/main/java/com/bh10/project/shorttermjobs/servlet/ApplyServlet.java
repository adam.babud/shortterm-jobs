
package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.exception.UserAlreadyAppliedException;
import com.bh10.project.shorttermjobs.service.ApplicationService;
import java.io.IOException;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ApplyServlet", urlPatterns = {"/apply"})
public class ApplyServlet extends HttpServlet {

    @Inject
    ApplicationService applicationService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        request.setAttribute("userAlreadyApplied", session.getAttribute("userAlreadyApplied"));
        request.setAttribute("userAppliedSuccess", session.getAttribute("userAppliedSuccess"));
        
        request.getRequestDispatcher("/WEB-INF/advertisement.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        session.setAttribute("userAlreadyApplied", false);
        session.setAttribute("userAppliedSuccess", false);
        
        Integer advertisementId = Integer.valueOf(request.getParameter("advertisementId"));
        Principal principal = request.getUserPrincipal();
        try {
            applicationService.createApplication(advertisementId, principal);
            session.setAttribute("userAppliedSuccess", true);
        } catch (UserAlreadyAppliedException ex) {
            session.setAttribute("userAlreadyApplied", true);
            Logger.getLogger(ApplyServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect("advertisements");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
