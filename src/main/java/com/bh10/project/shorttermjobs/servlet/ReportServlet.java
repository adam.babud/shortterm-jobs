
package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.service.AdvertisementService;
import com.bh10.project.shorttermjobs.service.ReportService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "ReportServlet", urlPatterns = {"/reports"})
public class ReportServlet extends HttpServlet {

    @Inject
    ReportService reportService;
    
    @Inject
    AdvertisementService advertisementService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("reportList", reportService.reportList());
        
        request.getRequestDispatcher("/WEB-INF/reports.jsp").forward(request,response);
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        if ("deleteAdvert".equals(request.getParameter("query"))) {
            Integer advertisementId = Integer.valueOf(request.getParameter("advertisementId"));
            advertisementService.deleteAdvertisementById(advertisementId);
            session.setAttribute("advertisement", null);
        }
        if((null != (request.getParameter("reportButton"))) && (request.getParameter("reportButton").equals("showAdvert"))){
            Integer reportId = Integer.valueOf(request.getParameter("reportId"));
            Integer advertisementId = reportService.findReportById(reportId).getAdvertisementDto().getId();
            AdvertisementDto advDto = advertisementService.findAdvertisementById(advertisementId);
            session.setAttribute("advertisement", advDto);
        }
        if((null != (request.getParameter("reportButton"))) && (request.getParameter("reportButton").equals("deleteReport"))){
            Integer reportId = Integer.valueOf(request.getParameter("reportId"));
            reportService.deleteReportById(reportId);
        }
        
        response.sendRedirect("reports");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
