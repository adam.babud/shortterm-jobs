package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.service.ImageUploadService;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@WebServlet(name = "FileUploadHandlerServlet", urlPatterns = {"/uploadProfile"})
public class FileUploadHandlerServlet extends HttpServlet {

    private static final String UPLOAD_DIRECTORY = "C:\\uploaded_files\\profile_images\\";
    private static final Integer MAX_FILE_SIZE_IN_BYTE = 1024 * 1024 * 10;

    @Inject
    private ImageUploadService imgUploadService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processCommonLogic(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processCommonLogic(request, response);
    }

    private void processCommonLogic(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("Bejövünk a doPost-ba");
        String file_name = null;
        response.setContentType("text/html; charset=UTF-8");
        
        boolean isMultipartContent = ServletFileUpload.isMultipartContent(request);
        if (!isMultipartContent) {
            System.out.println("Incorrect image format!");
            return;
        }
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding("UTF-8");
        try {
            List<FileItem> fields = upload.parseRequest(request);
            Iterator<FileItem> it = fields.iterator();
            if (!it.hasNext()) {
                return;
            }
            while (it.hasNext()) {
                FileItem fileItem = it.next();
                boolean isFormField = fileItem.isFormField();
                if (isFormField) {
                    if (file_name == null) {
                        if (fileItem.getFieldName().equals("file_name")) {
                            file_name = fileItem.getString();
                        }
                    }
                } else { // konstansok
                    if (fileItem.getSize() < MAX_FILE_SIZE_IN_BYTE
                            && (fileItem.getContentType().equals("image/jpeg")
                            || fileItem.getContentType().equals("image/jpg")
                            || fileItem.getContentType().equals("image/png")
                            || fileItem.getContentType().equals("image/bmp"))) {

                        String imageSourcePath = UPLOAD_DIRECTORY + fileItem.getName();
                        fileItem.write(new File(imageSourcePath));

                        String userEmail = request.getUserPrincipal().getName();

                        imgUploadService.persistProfileImagePath(userEmail, imageSourcePath);
                        response.sendRedirect("profile?query=myprofile");
                    } else {
                        request.getRequestDispatcher("WEB-INF/imageUploadError.jsp").include(request, response);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
