
package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.service.EmployerProfileService;
import java.io.IOException;
import java.security.Principal;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "EditCompanyProfileServlet", urlPatterns = {"/editcompanyprofile"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"user"}))
public class EditCompanyProfileServlet extends HttpServlet {

    @Inject
    EmployerProfileService employerProfileService;
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Principal principal = request.getUserPrincipal();
        request.setAttribute("company", employerProfileService.findEmployerDataByPrincipal(principal));
        request.getRequestDispatcher("/WEB-INF/editcompanyprofile.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Principal principal = request.getUserPrincipal();
        Integer dataId = employerProfileService.findEmployerDataByPrincipal(principal).getId();
        request.setCharacterEncoding("UTF-8");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        String size = request.getParameter("size");
        String description = request.getParameter("description");
        String businessScope = request.getParameter("businessScope");
        String hrContact = request.getParameter("hrContact");
        
        employerProfileService.updateEmployerDataByDataId(dataId, name, email, address, size, description, businessScope, hrContact);
        response.sendRedirect("profile?query=myprofile");
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
