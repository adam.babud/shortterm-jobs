package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.service.ProfileService;
import com.bh10.project.shorttermjobs.service.StjUserService;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "DisplayImageServlet", urlPatterns = {"/displayImage"})
public class DisplayImageServlet extends HttpServlet {

    @Inject
    private StjUserService stjUserService;

    @Inject
    private ProfileService profileService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        System.out.println("NAME PARAMETER ERTEKE: *****************************");
        System.out.println(request.getParameter("name"));
        System.out.println("NAME PARAMETER ERTEKE: *****************************");
        
        if (null != request.getParameter("name")) {
            
            response.setContentType("image/jpeg");
            String path = request.getParameter("name");
            ServletOutputStream out;
            out = response.getOutputStream();
            FileInputStream fin = new FileInputStream(path);
            BufferedInputStream bin = new BufferedInputStream(fin);
            BufferedOutputStream bout = new BufferedOutputStream(out);
            int ch = 0;
            while ((ch = bin.read()) != -1) {
                bout.write(ch);
            }
            bin.close();
            fin.close();
            bout.close();
            out.close();
            session.setAttribute("hasProfPic", true);
        } else {
            session.setAttribute("hasProfPic", false);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
