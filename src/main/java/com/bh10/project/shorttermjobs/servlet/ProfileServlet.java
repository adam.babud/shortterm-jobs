
package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.service.EmployerProfileService;
import com.bh10.project.shorttermjobs.service.ProfileService;
import com.bh10.project.shorttermjobs.service.StjUserService;
import java.io.IOException;
import java.security.Principal;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ProfileServlet", urlPatterns = {"/profile"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"user", "admin"}))
public class ProfileServlet extends HttpServlet {

    @Inject
    ProfileService profileService;

    @Inject
    EmployerProfileService employerProfileService;
    
    @Inject
    private StjUserService stjUserService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        

        Principal principal = request.getUserPrincipal();
        Integer id = 0;
            id = profileService.findUserByPrincipal(principal).getId();
            session.setAttribute("id", id);


        if ("myprofile".equals(request.getParameter("query"))) {
            if (profileService.getUserTypeIdByUserId(id) == 2) {
                //setUserProfilePicture(request);
                setProfileParameters(request);
                request.getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
            } else if (profileService.getUserTypeIdByUserId(id) == 3) {
                setCompanyProfileParameters(request);
                request.getRequestDispatcher("/WEB-INF/employerprofile.jsp").forward(request, response);
            }
        } else {
            Integer userId = Integer.valueOf(request.getParameter("query"));
            if (profileService.getUserTypeIdByUserId(userId) == 2) {
                //setUserProfilePicture(request, userId);
                setForeignUserProfileParameters(request, userId);
                request.getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
            } else if (profileService.getUserTypeIdByUserId(userId) == 3) {
                setForeignCompanyProfileParameters(request, userId);
                request.getRequestDispatcher("/WEB-INF/employerprofile.jsp").forward(request, response);
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void setProfileParameters(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("id");
        request.setAttribute("editable", true);
        request.setAttribute("data", profileService.findDataByUserId(id));
        request.setAttribute("hard", profileService.findHardSkillByUserId(id));
        request.setAttribute("soft", profileService.findSofSkillById(id));
        request.setAttribute("languages", profileService.findLanguageByUserId(id));
        request.setAttribute("experiences", profileService.findExperienceByUserId(id));
        request.setAttribute("educations", profileService.findEducationByUserId(id));
    }

    private void setCompanyProfileParameters(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("id");
        request.setAttribute("editable", true);
        request.setAttribute("company", employerProfileService.findEmployerDataByUserId(id));
        request.setAttribute("user", employerProfileService.findUserByUserId(id));
    }

    private void setForeignUserProfileParameters(HttpServletRequest request, Integer userId) {
        request.setAttribute("data", profileService.findDataByUserId(userId));
        request.setAttribute("hard", profileService.findHardSkillByUserId(userId));
        request.setAttribute("soft", profileService.findSofSkillById(userId));
        request.setAttribute("languages", profileService.findLanguageByUserId(userId));
        request.setAttribute("experiences", profileService.findExperienceByUserId(userId));
        request.setAttribute("educations", profileService.findEducationByUserId(userId));
    }

    private void setForeignCompanyProfileParameters(HttpServletRequest request, Integer userId) {
        request.setAttribute("company", employerProfileService.findEmployerDataByUserId(userId));
    }

}
