package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.entity.UserTypeEntity;
import com.bh10.project.shorttermjobs.exception.EmailIsUsedException;
import com.bh10.project.shorttermjobs.service.StjUserService;
import com.bh10.project.shorttermjobs.service.UserTypeService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "RegistrationServlet", urlPatterns = {"/registration"})
public class RegistrationServlet extends HttpServlet {    
    
    @Inject
    StjUserService stjUserService;

    @Inject
    UserTypeService userTypeService;
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<UserTypeEntity> userTypeList = userTypeService.listUserType();
        
        HttpSession session = request.getSession();
                 
        request.setAttribute("userTypeList", userTypeList);
        
        request.setAttribute("emailIsUsed", session.getAttribute("emailIsUsed"));
        
        request.setAttribute("registeredSuccessfully", session.getAttribute("registeredSuccessfully"));
        
        request.getRequestDispatcher("/WEB-INF/registration.jsp").forward(request, response);
               
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        
        session.setAttribute("emailIsUsed", false);
        
        session.setAttribute("registeredSuccessfully", false);
        
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String userTypeName = request.getParameter("userTypeName");
        
        try {
            stjUserService.createNewUser(email, name, password, userTypeService.findUserTypeByName(userTypeName));
            session.setAttribute("registeredSuccessfully", true);
        }catch (EmailIsUsedException emailIsUsedException){
            session.setAttribute("emailIsUsed", true);
            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, emailIsUsedException);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.sendRedirect("registration");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
