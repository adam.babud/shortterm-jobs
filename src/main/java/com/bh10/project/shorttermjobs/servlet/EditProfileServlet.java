
package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.service.ProfileService;
import java.io.IOException;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "EditProfileServlet", urlPatterns = {"/editprofile"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"user"}))
public class EditProfileServlet extends HttpServlet {

    @Inject
    ProfileService profileService;

    public static final Integer MAX_LEVEL = 100;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("id");
        if (profileService.getUserTypeIdByUserId(id) == 2) {
            request.setAttribute("data", profileService.findDataByUserId(id));
            request.setAttribute("hardSkills", profileService.findHardSkillByUserId(id));
            request.setAttribute("hard", profileService.findHardSkillByUserId(id));
            request.setAttribute(("softSkills"), profileService.findSofSkillById(id));
            request.setAttribute(("soft"), profileService.findSofSkillById(id));
            request.setAttribute("languages", profileService.findLanguageByUserId(id));
            request.setAttribute("experience", profileService.findExperienceByUserId(id));
            request.setAttribute("experiences", profileService.findExperienceByUserId(id));
            request.setAttribute("education", profileService.findEducationByUserId(id));
            request.setAttribute("educations", profileService.findEducationByUserId(id));
            request.getRequestDispatcher("/WEB-INF/editprofile.jsp").forward(request, response);
        }
        if (profileService.getUserTypeIdByUserId(id) == 3) {
            response.sendRedirect("editcompanyprofile");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("id");

        request.setCharacterEncoding("UTF-8");
        if ("datas".equals(request.getParameter("query"))) {
            updateData(request, response, id);
        }
        if ("updateHardSkills".equals(request.getParameter("query"))) {
            updateHardSkill(request, response, session);
        }
        if ("addHardSkills".equals(request.getParameter("query"))) {
            addHardSkill(request, response, id, session);
        }
        if ("updateSoftSkills".equals(request.getParameter("query"))) {
            updateSoftSkill(request, session, response);
        }
        if ("addSoftSkill".equals(request.getParameter("query"))) {
            addSoftSkill(request, response, id, session);
        }
        if ("updateLanguages".equals(request.getParameter("query"))) {
            updateLanguage(request, session, response);
        }
        if ("addLanguage".equals(request.getParameter("query"))) {
            addLanguage(request, response, id, session);
        }
        if ("updateExperience".equals(request.getParameter("query"))) {
            updateExperience(request, session, response);
        }
        if ("addExperince".equals(request.getParameter("query"))) {
            addExperience(request, response, id, session);
        }
        if ("updateEducation".equals(request.getParameter("query"))) {
            updateEducation(request, session, response);
        }
        if ("addEducation".equals(request.getParameter("query"))) {
            addEducation(request, response, id, session);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void updateData(HttpServletRequest request, HttpServletResponse response, Integer id) {
        String address = request.getParameter("address");
        String phoneNumber = request.getParameter("phoneNumber");
        String email = request.getParameter("cvEmail");
        String name = request.getParameter("cvName");
        profileService.updatePersonalDatas(id, address, phoneNumber, email, name);
        try {
            response.sendRedirect("editprofile");
        } catch (IOException ex) {
            Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateHardSkill(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        String updateName = request.getParameter("updateName");

        try {
            Integer updateLevel;
            if (Integer.valueOf(request.getParameter("updateLevel")) > MAX_LEVEL) {
                updateLevel = 100;
            } else {
                updateLevel = Integer.valueOf(request.getParameter("updateLevel"));
            }
            Integer hardSkillId = Integer.valueOf(request.getParameter("hardSkillId"));
            if (request.getParameter("hardSubmit").equals("update")) {
                profileService.updateHardSkillById(hardSkillId, updateName, updateLevel);
                session.setAttribute("invalidHardSkillLevel", false);
            } else {
                profileService.deleteHardSkill(hardSkillId);
            }
            try {
                response.sendRedirect("editprofile");
            } catch (IOException ex) {
                Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (NumberFormatException ex) {
            session.setAttribute("invalidHardSkillLevel", true);
            try {
                response.sendRedirect("editprofile");
            } catch (IOException ex1) {
                Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void addHardSkill(HttpServletRequest request, HttpServletResponse response, Integer id, HttpSession session) {
        String newName = request.getParameter("newName");
        try {
            Integer newLevel;
            if (Integer.valueOf(request.getParameter("newLevel")) > MAX_LEVEL) {
                newLevel = 100;
            } else {
                newLevel = Integer.valueOf(request.getParameter("newLevel"));
            }
            profileService.createNewHardSkillByJobApplicant(id, newName, newLevel);
            session.setAttribute("invalidHardSkillLevel", false);
            try {
                response.sendRedirect("editprofile");
            } catch (IOException ex) {
                Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NumberFormatException ex) {
            session.setAttribute("invalidHardSkillLevel", true);
            try {
                response.sendRedirect("editprofile");
            } catch (IOException ex1) {
                Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void updateSoftSkill(HttpServletRequest request, HttpSession session, HttpServletResponse response) {
        String updateName = request.getParameter("updateName");
        Integer softSkillId = Integer.valueOf(request.getParameter("softSkillId"));
        try {
            Integer updateLevel;
            if (Integer.valueOf(request.getParameter("updateLevel")) > MAX_LEVEL) {
                updateLevel = 100;
            } else {
                updateLevel = Integer.valueOf(request.getParameter("updateLevel"));
            }
            if (request.getParameter("softSubmit").equals("update")) {
                profileService.updateSoftSkillById(softSkillId, updateName, updateLevel);
                session.setAttribute("invalidHardSkillLevel", false);
            } else {
                profileService.deleteSoftSkill(softSkillId);
            }
            try {
                response.sendRedirect("editprofile");
            } catch (IOException ex) {
                Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NumberFormatException ex) {
            session.setAttribute("invalidHardSkillLevel", true);
            try {
                response.sendRedirect("editprofile");
            } catch (IOException ex1) {
                Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void addSoftSkill(HttpServletRequest request, HttpServletResponse response, Integer id, HttpSession session) {
        String newName = request.getParameter("newName");
        try {
            Integer newLevel;
            if (Integer.valueOf(request.getParameter("newLevel")) > MAX_LEVEL) {
                newLevel = 100;
            } else {
                newLevel = Integer.valueOf(request.getParameter("newLevel"));
            }
            profileService.createNewSoftSkillByJobApplicant(id, newName, newLevel);
            session.setAttribute("invalidHardSkillLevel", false);
            response.sendRedirect("editprofile");
        } catch (NumberFormatException ex) {
            session.setAttribute("invalidHardSkillLevel", true);
            try {
                response.sendRedirect("editprofile");
            } catch (IOException ex1) {
                Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (IOException ex) {
            Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateLanguage(HttpServletRequest request, HttpSession session, HttpServletResponse response) {
        String updateName = request.getParameter("updateName");
        String updateLevel = request.getParameter("updateLevel");
        Integer languageId = Integer.valueOf(request.getParameter("languageId"));
        if (request.getParameter("languageSubmit").equals("update")) {
            profileService.updateLanguageById(languageId, updateName, updateLevel);
            session.setAttribute("invalidHardSkillLevel", false);
        } else {
            profileService.deleteLanguage(languageId);
        }
        try {
            response.sendRedirect("editprofile");
        } catch (IOException ex) {
            Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addLanguage(HttpServletRequest request, HttpServletResponse response, Integer id, HttpSession session) {
        String newName = request.getParameter("newName");
        String newLevel = request.getParameter("newLevel");
        profileService.createNewLanguageByJobApplicant(id, newName, newLevel);
        session.setAttribute("invalidHardSkillLevel", false);
        try {
            response.sendRedirect("editprofile");
        } catch (IOException ex) {
            Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateExperience(HttpServletRequest request, HttpSession session, HttpServletResponse response) {
        String company = request.getParameter("updateCompany");
        String term = request.getParameter("updateTerm");
        String position = request.getParameter("updatePosition");
        String description = request.getParameter("updateDescription");
        Integer experienceId = Integer.valueOf(request.getParameter("experienceId"));
        if (request.getParameter("workSubmit").equals("update")) {
            profileService.updateExperienceById(experienceId, company, term, position, description);
            session.setAttribute("invalidHardSkillLevel", false);
        } else {
            profileService.deleteExperience(experienceId);
        }
        try {
            response.sendRedirect("editprofile");
        } catch (IOException ex) {
            Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addExperience(HttpServletRequest request, HttpServletResponse response, Integer id, HttpSession session) {
        String company = request.getParameter("newCompany");
        String term = request.getParameter("newTerm");
        String position = request.getParameter("newPosition");
        String description = request.getParameter("newDescription");
        profileService.createNewExperience(id, company, term, position, description);
        session.setAttribute("invalidHardSkillLevel", false);
        try {
            response.sendRedirect("editprofile");
        } catch (IOException ex) {
            Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateEducation(HttpServletRequest request, HttpSession session, HttpServletResponse response) {
        String school = request.getParameter("updateSchool");
        String qualification = request.getParameter("updateQualification");
        try {
            Date startDate = Date.valueOf(request.getParameter("updateStartDate"));
            Date endDate = Date.valueOf(request.getParameter("updateEndDate"));
            Integer educationId = Integer.valueOf(request.getParameter("educationId"));
            if (request.getParameter("eduSubmit").equals("update")) {
                profileService.updateEducationById(educationId, school, startDate, endDate, qualification);
                session.setAttribute("invalidHardSkillLevel", false);
            } else {
                profileService.deleteEducation(educationId);
            }
            response.sendRedirect("editprofile");
        } catch (IllegalArgumentException ex) {
            session.setAttribute("invalidHardSkillLevel", true);
            try {
                response.sendRedirect("editprofile");
            } catch (IOException ex1) {
                Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (IOException ex) {
            Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addEducation(HttpServletRequest request, HttpServletResponse response, Integer id, HttpSession session) {
        String school = request.getParameter("newSchool");
        String qualification = request.getParameter("newQualification");
        try {
            Date startDate = Date.valueOf(request.getParameter("newStartDate"));
            Date endDate = Date.valueOf(request.getParameter("newEndDate"));
            profileService.createNewEducation(id, school, startDate, endDate, qualification);
            session.setAttribute("invalidHardSkillLevel", false);
            response.sendRedirect("editprofile");
        } catch (IllegalArgumentException ex) {
            session.setAttribute("invalidHardSkillLevel", true);
            try {
                response.sendRedirect("editprofile");
            } catch (IOException ex1) {
                Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (IOException ex) {
            Logger.getLogger(EditProfileServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
