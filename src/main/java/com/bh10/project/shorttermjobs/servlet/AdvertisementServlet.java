
package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.service.AdvertisementService;
import com.bh10.project.shorttermjobs.service.ProfileService;
import com.bh10.project.shorttermjobs.service.StjUserService;
import java.io.IOException;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AdvertisementServlet", urlPatterns = {"/advertisements"})
@DeclareRoles({"user", "admin"})
public class AdvertisementServlet extends HttpServlet {

    @Inject
    AdvertisementService service;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        List<AdvertisementDto> list = service.findAllAdvertisement();
        request.setCharacterEncoding("UTF-8");
        String jobTypeOrLocation = request.getParameter("query");
        if (!service.getCategorySearch(jobTypeOrLocation).isEmpty()) {
            request.setAttribute("advertisements", service.getCategorySearch(jobTypeOrLocation));
        } else if (!service.getLocationSearch(jobTypeOrLocation).isEmpty()) {
            request.setAttribute("advertisements", service.getLocationSearch(jobTypeOrLocation));
        } else {
            request.setAttribute("advertisements", list);
        }    
        setCategoryAndLocation(request);
        request.getRequestDispatcher("/WEB-INF/advertisement.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<AdvertisementDto> list = service.findAllAdvertisement();
        request.setCharacterEncoding("UTF-8");
        if ("search".equals(request.getParameter("query"))) {
            String jobType = request.getParameter("jobType");
            String location = request.getParameter("location");

            request.setAttribute("advertisements", service.getSearchResult(jobType, location));
        }
        setCategoryAndLocation(request);
        request.getRequestDispatcher("/WEB-INF/advertisement.jsp").forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
    private void setCategoryAndLocation(HttpServletRequest request) {
        List<AdvertisementDto> list = service.findAllAdvertisement();
        request.setAttribute("categories", service.getCategoriesSet(list));
        request.setAttribute("locations", service.getLocationSet(list));
    }
    
    
}
