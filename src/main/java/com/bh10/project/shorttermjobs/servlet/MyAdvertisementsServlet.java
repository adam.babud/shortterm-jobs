package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.service.AdvertisementService;
import com.bh10.project.shorttermjobs.service.ApplicationService;
import com.bh10.project.shorttermjobs.service.ProfileService;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MyAdvertisementsServlet", urlPatterns = {"/myAdvertisements"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"user"}))
public class MyAdvertisementsServlet extends HttpServlet {

    @Inject
    ProfileService profileService;

    
    @Inject
    AdvertisementService advertisementService;

    @Inject
    ApplicationService applicationService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Principal principal = request.getUserPrincipal();
        Integer id = profileService.findUserByPrincipal(principal).getId();
        List<AdvertisementDto> advertisements = advertisementService.getUserAdvertisements(profileService.findUserById(id));
        

        request.setAttribute("myads", advertisements);
        request.getRequestDispatcher("/WEB-INF/myads.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameter("deleteAdvert").equals("delete")) {
            Integer id = Integer.valueOf(request.getParameter("advertisementId"));
            advertisementService.deleteAdvertisementById(id);          
        }
        response.sendRedirect("myAdvertisements");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
