
package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.dto.StjUserDto;
import com.bh10.project.shorttermjobs.exception.UserAlreadyReportedThisException;
import com.bh10.project.shorttermjobs.service.AdvertisementService;
import com.bh10.project.shorttermjobs.service.ReportService;
import com.bh10.project.shorttermjobs.service.StjUserService;
import java.io.IOException;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ReportAdvertisementServlet", urlPatterns = {"/reportAdvertisement"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"user"}))
public class ReportAdvertisementServlet extends HttpServlet {
   
    @Inject
    ReportService reportService;
    
    @Inject
    StjUserService stjUserService;  
    
    @Inject
    AdvertisementService advertisementService;
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        session.setAttribute("advertisementId", Integer.valueOf(request.getParameter("query")));
        
        AdvertisementDto advertisement = advertisementService.findAdvertisementById(Integer.valueOf(request.getParameter("query")));
        
        request.setAttribute("advertisement", advertisement);
        
        request.getRequestDispatcher("/WEB-INF/reportAdvertisement.jsp").forward(request, response);
      
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        Integer advertisementId = (Integer) session.getAttribute("advertisementId");
                
        String descriptionOfReport = request.getParameter("descriptionOfReport");
        Principal userPrincipal = request.getUserPrincipal();
        
        AdvertisementDto advertisement = advertisementService.findAdvertisementById(advertisementId);
        
        StjUserDto user = stjUserService.getUserByEmail(userPrincipal.getName());
        
        try {
            reportService.addNewReport(descriptionOfReport, user, advertisement);
        } catch (UserAlreadyReportedThisException ex) {
            Logger.getLogger(ReportAdvertisementServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("advertisements");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
