
package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.exception.EmailIsUsedException;
import com.bh10.project.shorttermjobs.service.StjUserService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "CreateAdminServlet", urlPatterns = {"/createAdmin"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin"}))
public class CreateAdminServlet extends HttpServlet {
    
    @Inject
    StjUserService stjUserService;
           
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        request.setAttribute("emailIsUsed", session.getAttribute("emailIsUsed"));
        
        request.setAttribute("registeredSuccessfully", session.getAttribute("registeredSuccessfully"));
        
        request.getRequestDispatcher("/WEB-INF/createAdmin.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        
        session.setAttribute("emailIsUsed", false);
        
        session.setAttribute("registeredSuccessfully", false);
        
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        
        try {
            stjUserService.createNewAdmin(email, name, password);
            session.setAttribute("registeredSuccessfully", true);
        }catch (EmailIsUsedException emailIsUsedException){
            session.setAttribute("emailIsUsed", true);
            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, emailIsUsedException);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.sendRedirect("createAdmin");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
