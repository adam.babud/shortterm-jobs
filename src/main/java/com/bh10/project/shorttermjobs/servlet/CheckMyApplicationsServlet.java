
package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.exception.UserHasNoApplicationException;
import com.bh10.project.shorttermjobs.service.ApplicationService;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CheckMyApplicationsServlet", urlPatterns = {"/checkmyapplications"})
public class CheckMyApplicationsServlet extends HttpServlet {

    
    @Inject
    ApplicationService applicationService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        List<AdvertisementDto> advertisementsAppliedByUserList = new ArrayList<>();
        Principal principal = request.getUserPrincipal();
        boolean userHasNoApplication = false;
        
        
        try {
            advertisementsAppliedByUserList = applicationService.getAdvertisementAppliedByUser(applicationService.findUserByPrincipal(principal).getId());
       } catch (UserHasNoApplicationException ex) {
           userHasNoApplication = true;
           Logger.getLogger(CheckMyApplicationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        request.setAttribute("userHasNoApplication", userHasNoApplication);
        request.setAttribute("advertisements", advertisementsAppliedByUserList);
        
        request.getRequestDispatcher("/WEB-INF/checkmyapplications.jsp").forward(request, response);
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
