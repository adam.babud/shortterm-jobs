
package com.bh10.project.shorttermjobs.servlet;

import com.bh10.project.shorttermjobs.service.AdvertisementService;
import com.bh10.project.shorttermjobs.service.ProfileService;
import java.io.IOException;
import java.security.Principal;
import java.sql.Date;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "CreateAdvertisementServlet", urlPatterns = {"/createadvertisement"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"user"}))
public class CreateAdvertisementServlet extends HttpServlet {

    @Inject
    AdvertisementService advertisementService;
    
    @Inject
    ProfileService profileService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Principal principal = request.getUserPrincipal();
        HttpSession session = request.getSession();
        request.setAttribute("id", profileService.findUserByPrincipal(principal).getId());
        request.setAttribute("user", profileService.findUserByPrincipal(principal));
        request.getRequestDispatcher("/WEB-INF/createadvertisement.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        session.setAttribute("dateFormat", false);
        Principal principal = request.getUserPrincipal();
        Integer userId = profileService.findUserByPrincipal(principal).getId();
       String jobType = request.getParameter("jobType");
       String location = request.getParameter("location");
       String description = request.getParameter("description");
       String category = request.getParameter("category");
       try{
       Date date = Date.valueOf(request.getParameter("date"));
       advertisementService.createAdvertisement(userId, jobType, location, date, description, category);
       response.sendRedirect("myAdvertisements");
       } catch(IllegalArgumentException ex) {
           session.setAttribute("dateFormat", true);
           response.sendRedirect("createadvertisement");
       }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
