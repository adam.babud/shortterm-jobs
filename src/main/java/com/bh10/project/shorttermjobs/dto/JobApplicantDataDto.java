package com.bh10.project.shorttermjobs.dto;

public class JobApplicantDataDto {

    private Integer id;
    private String cvEmail;
    private String cvName;
    private String address;
    private String phoneNumber;
    private String description;
    private String cvUrl;
    private String profImgPath;

    public JobApplicantDataDto() {
    }

    public JobApplicantDataDto(Integer id, String cvEmail, String cvName, String address, String phoneNumber, String description, String cvUrl, String profImgPath) {
        this.id = id;
        this.cvEmail = cvEmail;
        this.cvName = cvName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.description = description;
        this.cvUrl = cvUrl;
        this.profImgPath = profImgPath;
    }

    public String getProfImgPath() {
        return profImgPath;
    }

    public void setProfImgPath(String profImgPath) {
        this.profImgPath = profImgPath;
    }

    public String getCvEmail() {
        return cvEmail;
    }

    public void setCvEmail(String cvEmail) {
        this.cvEmail = cvEmail;
    }

    public String getCvName() {
        return cvName;
    }

    public void setCvName(String cvName) {
        this.cvName = cvName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCvUrl() {
        return cvUrl;
    }

    public void setCvUrl(String cvUrl) {
        this.cvUrl = cvUrl;
    }

    @Override
    public String toString() {
        return "JobApplicantDataDto{" + "id=" + id + ", address=" + address + ", phoneNumber=" + phoneNumber + ", description=" + description + ", cvUrl=" + cvUrl + '}';
    }

}
