package com.bh10.project.shorttermjobs.dto;

import java.sql.Date;

public class EducationDto {

    private Integer id;
    private Date startDate;
    private Date endDate;
    private String school;
    private String qualification;

    public EducationDto() {
    }

    public EducationDto(Integer id, Date startDate, Date endDate, String school, String qualification) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.school = school;
        this.qualification = qualification;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @Override
    public String toString() {
        return "EducationDto{" + "id=" + id + ", startDate=" + startDate + ", endDate=" + endDate + ", school=" + school + ", qualification=" + qualification + '}';
    }

}
