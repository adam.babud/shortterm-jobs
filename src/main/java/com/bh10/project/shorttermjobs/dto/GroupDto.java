package com.bh10.project.shorttermjobs.dto;

public class GroupDto {

    private Integer id;
    private String name;

    public GroupDto() {
    }

    public GroupDto(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "GroupDto{" + "id=" + id + ", name=" + name + '}';
    }

}
