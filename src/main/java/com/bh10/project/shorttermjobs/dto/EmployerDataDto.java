package com.bh10.project.shorttermjobs.dto;

public class EmployerDataDto {

    private Integer id;
    private String hrContact;
    private String address;
    private String phoneNumber;
    private String size;
    private String description;
    private String businessScope;
    private String email;
    private String name;

    public EmployerDataDto() {
    }

    public EmployerDataDto(Integer id, String hrContact, String address, String phoneNumber, String size, String description, String businessScope, String email, String name) {
        this.id = id;
        this.hrContact = hrContact;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.size = size;
        this.description = description;
        this.businessScope = businessScope;
        this.email = email;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHrContact() {
        return hrContact;
    }

    public void setHrContact(String hrContact) {
        this.hrContact = hrContact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBusinessScope() {
        return businessScope;
    }

    public void setBusinessScope(String businessScope) {
        this.businessScope = businessScope;
    }

    @Override
    public String toString() {
        return "EmployerDataDto{" + "id=" + id + ", hrContact=" + hrContact + ", address=" + address + ", phoneNumber=" + phoneNumber + ", size=" + size + ", description=" + description + ", businessScope=" + businessScope + '}';
    }

}
