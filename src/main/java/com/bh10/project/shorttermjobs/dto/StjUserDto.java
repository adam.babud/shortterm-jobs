package com.bh10.project.shorttermjobs.dto;

public class StjUserDto {

    private Integer id;
    private String email;
    private String name;
    private String password;

    public StjUserDto() {
    }

    public StjUserDto(Integer id, String email, String name, String password) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "StjUserDto{" + "id=" + id + ", email=" + email + ", name=" + name + ", password=" + password + '}';
    }

}
