package com.bh10.project.shorttermjobs.dto;

public class LanguageDto {

    private Integer id;
    private String name;
    private String level;

    public LanguageDto() {
    }

    public LanguageDto(Integer id, String name, String level) {
        this.id = id;
        this.name = name;
        this.level = level;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "LanguageDto{" + "id=" + id + ", name=" + name + ", level=" + level + '}';
    }

}
