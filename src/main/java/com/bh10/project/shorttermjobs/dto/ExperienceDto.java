package com.bh10.project.shorttermjobs.dto;

public class ExperienceDto {

    private Integer id;
    private String company;
    private String term;
    private String position;
    private String description;

    public ExperienceDto() {
    }

    public ExperienceDto(Integer id, String company, String term, String position, String description) {
        this.id = id;
        this.company = company;
        this.term = term;
        this.position = position;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ExperienceDto{" + "id=" + id + ", company=" + company + ", term=" + term + ", position=" + position + '}';
    }

}
