package com.bh10.project.shorttermjobs.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "hard_skill")
public class HardSkillEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    private String name;

    private Integer level;

    @OneToOne
    @JoinColumn(name = "job_applicant_data_id")
    private JobApplicantDataEntity jobApplicantData;

    public HardSkillEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public JobApplicantDataEntity getJobApplicantData() {
        return jobApplicantData;
    }

    public void setJobApplicantData(JobApplicantDataEntity jobApplicantData) {
        this.jobApplicantData = jobApplicantData;
    }

}
