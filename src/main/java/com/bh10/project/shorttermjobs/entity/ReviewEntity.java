package com.bh10.project.shorttermjobs.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "review")
public class ReviewEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    private String description;

    private Integer score;

    @ManyToOne
    @JoinColumn(name = "reviewer_user_id")
    private StjUserEntity reviewerUser;

    @ManyToOne
    @JoinColumn(name = "reviewed_user_id")
    private StjUserEntity reviewedUser;

    public ReviewEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public StjUserEntity getReviewerUser() {
        return reviewerUser;
    }

    public void setReviewerUser(StjUserEntity reviewerUser) {
        this.reviewerUser = reviewerUser;
    }

    public StjUserEntity getReviewedUser() {
        return reviewedUser;
    }

    public void setReviewedUser(StjUserEntity reviewedUser) {
        this.reviewedUser = reviewedUser;
    }

}
