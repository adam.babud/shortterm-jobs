package com.bh10.project.shorttermjobs.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user_group")
public class GroupEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "group")
    private List<StjUserEntity> usersList;

    @ManyToMany(mappedBy = "groups")
    private List<StjUserEntity> users;

    public List<StjUserEntity> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<StjUserEntity> usersList) {
        this.usersList = usersList;
    }

    public GroupEntity() {
    }

    public List<StjUserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<StjUserEntity> users) {
        this.users = users;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
