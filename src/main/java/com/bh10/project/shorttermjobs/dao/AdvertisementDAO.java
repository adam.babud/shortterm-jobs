
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.entity.AdvertisementEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class AdvertisementDAO {

    @PersistenceContext
    EntityManager em;

    public List<AdvertisementEntity> findAllAdvertisement() {
        return (List<AdvertisementEntity>) em.createQuery("select a from AdvertisementEntity a")
                .getResultList();
    }
    
    public List<AdvertisementEntity> findAdvertisementByJobTypeAndLocation(String jobType, String location) {
        return (List<AdvertisementEntity>) em.createQuery("select a from AdvertisementEntity a where a.jobType LIKE :jobType AND a.location LIKE :location")
                .setParameter("jobType", "%" + jobType + "%")
                .setParameter("location", "%" + location + "%") 
                .getResultList();
    }
    
    public List<AdvertisementEntity> findAdvertisementByCategory(String category) {
        return (List<AdvertisementEntity>) em.createQuery("select a from AdvertisementEntity a where a.category = :category")
                .setParameter("category", category)
                .getResultList();
    }
    
     public List<AdvertisementEntity> findAdvertisementsByUser(StjUserEntity entity) {
        return (List<AdvertisementEntity>) em.createQuery("select a from AdvertisementEntity a where a.user = :user")
                .setParameter("user", entity)
                .getResultList();
    }
    
     
     public void createAdvertisement(AdvertisementEntity entity) {
         em.persist(entity);
     }
     
     public void deleteAdvertisementById(Integer id) {
         em.remove(findAdvertisementById(id));
     }
     
     public AdvertisementEntity findAdvertisementById(Integer id){
        return em.find(AdvertisementEntity.class, id);
    }
}
