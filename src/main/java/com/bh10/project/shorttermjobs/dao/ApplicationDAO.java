
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.AdvertisementEntity;
import com.bh10.project.shorttermjobs.entity.ApplicationEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
public class ApplicationDAO {

    @PersistenceContext
    EntityManager em;

    private static final String LIST_REPORTS_BY_USERID = "SELECT a FROM ApplicationEntity a WHERE a.user.id = :stjUserId";
    
    public void createApplication(ApplicationEntity entity) {
        em.persist(entity);
    }
    
    public List<ApplicationEntity> applicationListByUserId(Integer stjUserId){
        
        List<ApplicationEntity> applicationList = null;
        
        applicationList = em.createQuery(LIST_REPORTS_BY_USERID)
                            .setParameter("stjUserId", stjUserId)
                            .getResultList();
        
        return applicationList;
    }

    public List<ApplicationEntity> getApplicantForAdvertisement(AdvertisementEntity entity) {
        return (List<ApplicationEntity>) em.createQuery("select a from ApplicationEntity a where a.advertisement = :advertisement")
                .setParameter("advertisement", entity)
                .getResultList();
    }
    public List<ApplicationEntity> getApplicantForAdvertisementId(Integer id) {
        return (List<ApplicationEntity>) em.createQuery("select a from ApplicationEntity a where a.advertisement.id = :id")
                .setParameter("id", id)
                .getResultList();
    }
    
    
}
