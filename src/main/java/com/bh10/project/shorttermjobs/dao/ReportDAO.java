
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.AdvertisementEntity;
import com.bh10.project.shorttermjobs.entity.ReportEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Stateless
public class ReportDAO {
    
    @PersistenceContext
    EntityManager em;
    
    private static final String ADD_NEW_REPORT_STATUS = "waitingForReview";
    private static final String FIND_REPORTS_BY_USER = "SELECT r FROM ReportEntity r WHERE r.reporterUser.id = :stjUserId";
    private static final String FIND_REPORTS = "SELECT r FROM ReportEntity r";
    
    @Transactional
    public void addNewReport(String reportDescription, StjUserEntity user, AdvertisementEntity advertisement){
        ReportEntity report = new ReportEntity();
        
        report.setDescription(reportDescription);
        report.setStatus(ADD_NEW_REPORT_STATUS);
        report.setAdvertisement(advertisement);
        report.setReporterUser(user);
        
        em.persist(report);
        
    }

    public List<ReportEntity> reportListByUserId(Integer stjUserId){
        
        List<ReportEntity> reportList = null;

        reportList= em.createQuery(FIND_REPORTS_BY_USER)
                      .setParameter("stjUserId", stjUserId)
                      .getResultList();

        return reportList;
    }
    
    public List<ReportEntity> reportList(){
        return em.createQuery(FIND_REPORTS)
                 .getResultList();
    }
    
    public void deleteReportById(Integer reportId) {
        em.remove(findReportById(reportId));
    }
    
    public ReportEntity findReportById(Integer reportId) {
        return em.find(ReportEntity.class, reportId);
    }
    
}
