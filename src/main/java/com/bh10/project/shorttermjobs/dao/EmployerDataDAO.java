
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.EmployerDataEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
public class EmployerDataDAO {

    @PersistenceContext
    EntityManager em;

    public EmployerDataEntity findDataByUser(StjUserEntity entity) {
        return (EmployerDataEntity) em.createQuery("select d from EmployerDataEntity d where d.user = :user")
                .setParameter("user", entity)
                .getSingleResult();
    }

    public EmployerDataEntity findDataByDataId(Integer id) {
        return (EmployerDataEntity) em.createQuery("select d from EmployerDataEntity d where d.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }
    
    public void updateEmployerData(EmployerDataEntity entity) {
        em.merge(entity);
    }
    
    public void createData(EmployerDataEntity employerDataEntity) {
        em.persist(employerDataEntity);
    }
}
