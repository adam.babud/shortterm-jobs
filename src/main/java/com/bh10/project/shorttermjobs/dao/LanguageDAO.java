
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import com.bh10.project.shorttermjobs.entity.LanguageEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
public class LanguageDAO {

    @PersistenceContext
    EntityManager em;

    public List<LanguageEntity> findLanguageByJobApplicant(JobApplicantDataEntity jobApplicantDataEntity) {
        return (List<LanguageEntity>) em.createQuery("select l from LanguageEntity l where l.jobApplicantData = :id")
                .setParameter("id", jobApplicantDataEntity)
                .getResultList();
    }

    public LanguageEntity findLanguageById(Integer id) {
        return (LanguageEntity) em.createQuery("select l from LanguageEntity l where l.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }

    public void updateLanguage(LanguageEntity entity) {
        em.merge(entity);
    }

    public void addNewLanguage(LanguageEntity entity) {
        em.persist(entity);
    }
    
    public void deleteLanguage(LanguageEntity entity) {
        em.remove(entity);
    }
}
