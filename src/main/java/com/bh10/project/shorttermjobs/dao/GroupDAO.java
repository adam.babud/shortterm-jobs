package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.GroupEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class GroupDAO {

    @PersistenceContext
    EntityManager em;

    private static final String FIND_GROUP = "SELECT g FROM GroupEntity g WHERE g.name = :name";
    
    public List<GroupEntity> listGroups() {
       return em.createQuery("select g from GroupEntity g").getResultList();
    }
    
    public GroupEntity findGroupByName(String name){
        return (GroupEntity) em.createQuery(FIND_GROUP)
                .setParameter("name", name)
                .getSingleResult();
    }
    
}
