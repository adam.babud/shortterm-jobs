
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.GroupEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import com.bh10.project.shorttermjobs.entity.UserTypeEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Stateless
public class StjUserDAO {
    
    @PersistenceContext
    EntityManager em;
    
    private static final String GET_USER_BY_EMAIL = "SELECT u FROM StjUserEntity u WHERE u.email = :email";
    private static final String GET_COUNT_OF_USERS_BY_EMAIL = "SELECT count(u.id) FROM StjUserEntity u WHERE u.email = :email";
    private static final String GET_USER_BY_ID = "SELECT u FROM StjUserEntity u WHERE u.id = :id";
    
    public StjUserEntity getStjUserById(Integer id) {
        return (StjUserEntity) em.createQuery(GET_USER_BY_ID)
                .setParameter("id", id)
                .getSingleResult();
    }

    public StjUserEntity getUserByEmail(String email) {
        return (StjUserEntity) em.createQuery(GET_USER_BY_EMAIL)
                .setParameter("email", email)
                .getSingleResult();
    }
    
    public Long getCountOfUsersByEmail(String email) {
        return (Long) em.createQuery(GET_COUNT_OF_USERS_BY_EMAIL)
                .setParameter("email", email)
                .getSingleResult();
    }
    
    @Transactional
    public void createNewUser(String email, String name, String password, UserTypeEntity userTypeEntity, GroupEntity group){
        StjUserEntity user = new StjUserEntity();
        
        user.setEmail(email);
        user.setName(name);
        user.setPassword(password);
        user.setUserType(userTypeEntity);
        user.setGroup(group);
        user.getGroups().add(group);
        
        em.persist(user);
    }
    
    public void updateEmailAndName(StjUserEntity entity) {
        em.merge(entity);
    }
    
}
