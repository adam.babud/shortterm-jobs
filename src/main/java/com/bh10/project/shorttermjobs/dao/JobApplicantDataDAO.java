
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
public class JobApplicantDataDAO {
    
    @PersistenceContext
    EntityManager em;
    
    public JobApplicantDataEntity findDataById(Integer id) {
        return (JobApplicantDataEntity) em.createQuery("select j from JobApplicantDataEntity j where j.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }
    
    public void updatePersonalDatas(JobApplicantDataEntity entity) {
        em.merge(entity);
    }
    
    public JobApplicantDataEntity findDataByUser(StjUserEntity entity) {
        return (JobApplicantDataEntity) em.createQuery("select j from JobApplicantDataEntity j where j.userId = :userId")
                .setParameter("userId", entity)
                .getSingleResult();
    }
    
    public void persistData(JobApplicantDataEntity jobApplicantDataEntity) {
        em.persist(jobApplicantDataEntity);
    }
}
