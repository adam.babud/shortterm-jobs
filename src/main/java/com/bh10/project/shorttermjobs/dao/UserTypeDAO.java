
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.UserTypeEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class UserTypeDAO {
    
    @PersistenceContext
    EntityManager em;
    
    private static final String FIND_USER_TYPE_BY_NAME = "SELECT u FROM UserTypeEntity u WHERE u.name = :name";
    private static final String LIST_USER_TYPE = "SELECT u FROM UserTypeEntity u WHERE NOT u.name = :name";
    
    public UserTypeEntity findUserTypeByName(String name){
        return (UserTypeEntity) em.createQuery(FIND_USER_TYPE_BY_NAME)
                .setParameter("name", name)
                .getSingleResult();

        
    }
    
    public List<UserTypeEntity> listUserType(){
        return em.createQuery(LIST_USER_TYPE)
                .setParameter("name", "admin")
                .getResultList();
    }

    
}
