
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.ExperienceEntity;
import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ExperienceDAO {

    @PersistenceContext
    EntityManager em;

    public List<ExperienceEntity> findExperienceByJobApplicant(JobApplicantDataEntity jobApplicantDataEntity) {
        return (List<ExperienceEntity>) em.createQuery("select x from ExperienceEntity x where x.jobApplicantData = :id")
                .setParameter("id", jobApplicantDataEntity)
                .getResultList();
    }

    public ExperienceEntity findExperienceById(Integer id) {
        return (ExperienceEntity) em.createQuery("select e from ExperienceEntity e where e.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }

    public void updateExperience(ExperienceEntity entity) {
        em.merge(entity);
    }

    public void addNewExperience(ExperienceEntity entity) {
        em.persist(entity);
    }
    
    public void deleteExperience(ExperienceEntity entity) {
        em.remove(entity);
    }
}
