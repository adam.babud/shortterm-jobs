
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.EducationEntity;
import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
public class EducationDAO {

    @PersistenceContext
    EntityManager em;

    public List<EducationEntity> findEducationByJobApplicant(JobApplicantDataEntity jobApplicantDataEntity) {
        return (List<EducationEntity>) em.createQuery("select e from EducationEntity e where e.jobApplicantData = :id")
                .setParameter("id", jobApplicantDataEntity)
                .getResultList();
    }

    public EducationEntity findEducationById(Integer id) {
        return (EducationEntity) em.createQuery("select e from EducationEntity e where e.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }
    
    public void updateEducation(EducationEntity entity) {
        em.merge(entity);
    }
    
    public void addNewEducation(EducationEntity entity) {
        em.persist(entity);
    }
    
    public void deleteEducation(EducationEntity entity) {
        em.remove(entity);
    }
}
