
package com.bh10.project.shorttermjobs.exception;

public class UserHasNoApplicationException extends Exception{

    @Override
    public String getMessage() {
        return "User has no application yet.";
    }
    
    
}
