
package com.bh10.project.shorttermjobs.exception;

public class UserAlreadyAppliedException extends Exception{

    public String getMessage(String userEmail, String advertisementID) {
        return "User: " + userEmail + " is already applied for advertisement: " + advertisementID;
    }
    
}
