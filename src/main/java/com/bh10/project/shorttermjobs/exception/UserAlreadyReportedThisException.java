
package com.bh10.project.shorttermjobs.exception;

public class UserAlreadyReportedThisException extends Exception{

    @Override
    public String getMessage() {
        return "This user already reported this advertisement";
    }
    
}
