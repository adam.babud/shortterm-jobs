
package com.bh10.project.shorttermjobs.exception;

public class EmailIsUsedException extends Exception{

    @Override
    public String getMessage() {
        return "E-mail is already used by another user!";
    }
    
}
