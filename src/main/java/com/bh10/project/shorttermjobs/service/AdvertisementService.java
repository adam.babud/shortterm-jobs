
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.AdvertisementDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.dto.ApplicationDto;
import com.bh10.project.shorttermjobs.dto.StjUserDto;
import com.bh10.project.shorttermjobs.entity.AdvertisementEntity;
import com.bh10.project.shorttermjobs.mapper.AdvertisementMapper;
import com.bh10.project.shorttermjobs.mapper.StjUserMapper;
import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class AdvertisementService {

    @Inject
    AdvertisementDAO advertisementDAO;

    @Inject
    ApplicationService applicationService;

    @Inject
    StjUserDAO stjUserDAO;

    public AdvertisementService(AdvertisementDAO advertisementDAO, ApplicationService applicationService, StjUserDAO stjUserDAO) {
        this.advertisementDAO = advertisementDAO;
        this.applicationService = applicationService;
        this.stjUserDAO = stjUserDAO;
    }

    
    public AdvertisementService() {
    }

    public List<StjUserDto> findAdvertiserByAdvertisement(List<AdvertisementDto> list) {
        List<StjUserDto> dtoList = new ArrayList<>();
        list.forEach((advertisementDto) -> {
            dtoList.add(advertisementDto.getStjUserDto());
        });
        return dtoList;
    }

    public void deleteAdvertisementById(Integer id) {
        advertisementDAO.deleteAdvertisementById(id);
    }

    public List<AdvertisementDto> findAllAdvertisement() {
        List<AdvertisementEntity> list = advertisementDAO.findAllAdvertisement();
        List<AdvertisementDto> dtoList = new ArrayList<>();
        list.forEach((advertisementEntity) -> {
            dtoList.add(AdvertisementMapper.toDto(advertisementEntity));
        });
        return dtoList;
    }

    public List<AdvertisementDto> getAllAdvertisementByJobTypeAndLocation(String jobType, String location) {
        List<AdvertisementEntity> list = advertisementDAO.findAdvertisementByJobTypeAndLocation(jobType, location);
        List<AdvertisementDto> dtoList = new ArrayList<>();
        list.forEach((advertisementEntity) -> {
            dtoList.add(AdvertisementMapper.toDto(advertisementEntity));
        });
        return dtoList;
    }

    public List<AdvertisementDto> getSearchResult(String jobType, String location) {
        return getAllAdvertisementByJobTypeAndLocation(jobType, location);
    }

    public List<AdvertisementDto> getCategorySearch(String category) {
        List<AdvertisementEntity> list = advertisementDAO.findAdvertisementByCategory(category);
        List<AdvertisementDto> dtos = new ArrayList<>();
        list.forEach((p) -> {
            dtos.add(AdvertisementMapper.toDto(p));
        });
        return dtos;
    }

    public List<AdvertisementDto> getLocationSearch(String location) {
        return getSearchResult("", location);
    }

    public Set<String> getCategoriesSet(List<AdvertisementDto> list) {
        Set<String> categories = new LinkedHashSet<>();
        list.forEach((advertisementDto) -> {
            categories.add(advertisementDto.getCategory());
        });
        return categories;
    }

    public Set<String> getLocationSet(List<AdvertisementDto> list) {
        Set<String> locations = new LinkedHashSet<>();
        list.forEach((advertisementDto) -> {
            locations.add(advertisementDto.getLocation());
        });
        return locations;
    }

    public AdvertisementDto findAdvertisementById(Integer id) {
        return AdvertisementMapper.toDto(advertisementDAO.findAdvertisementById(id));
    }

    public List<AdvertisementDto> getUserAdvertisements(StjUserDto entity) {
        List<AdvertisementEntity> list = advertisementDAO.findAdvertisementsByUser(StjUserMapper.toEntity(entity));
        List<AdvertisementDto> dtoList = new ArrayList<>();
        list.forEach((advertisementEntity) -> {
            List<ApplicationDto> applications = applicationService.findApplicantByAdvertisement(advertisementEntity);
            AdvertisementDto advDto = AdvertisementMapper.toDto(advertisementEntity);
            advDto.setApplications(applications);
            dtoList.add(advDto);
        });
        return dtoList;
    }

    public List<ApplicationDto> getApplicantByAdvertisementId(Integer id) {
        AdvertisementEntity entity = applicationService.findAdvertisementById(id);
        return applicationService.findApplicantByAdvertisement(entity);
    }

    public void createAdvertisement(Integer userId, String jobType, String location, Date date, String description, String category) {
        AdvertisementEntity entity = new AdvertisementEntity();
        entity.setUser(stjUserDAO.getStjUserById(userId));
        entity.setDate(date);
        entity.setDescription(description);
        entity.setJobType(jobType);
        entity.setLocation(location);
        entity.setCategory(category);
        advertisementDAO.createAdvertisement(entity);
    }
}
