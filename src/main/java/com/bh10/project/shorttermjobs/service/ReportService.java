
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.ReportDAO;
import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.dto.ReportDto;
import com.bh10.project.shorttermjobs.dto.StjUserDto;
import com.bh10.project.shorttermjobs.entity.ReportEntity;
import com.bh10.project.shorttermjobs.exception.UserAlreadyReportedThisException;
import com.bh10.project.shorttermjobs.mapper.AdvertisementMapper;
import com.bh10.project.shorttermjobs.mapper.ReportMapper;
import com.bh10.project.shorttermjobs.mapper.StjUserMapper;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class ReportService {
    
    @Inject
    ReportDAO reportDAO;

    public ReportService() {
    }

    public ReportService(ReportDAO reportDAO) {
        this.reportDAO = reportDAO;
    }
    
    
    
    public void addNewReport(String reportDescription, StjUserDto user, AdvertisementDto advertisement) throws UserAlreadyReportedThisException{
        
        List<ReportEntity> reportListByUser = null;
            reportListByUser = reportDAO.reportListByUserId(user.getId());
        
        if(null != reportListByUser){
            for (ReportEntity reportEntity : reportListByUser) {
                if(reportEntity.getAdvertisement().getId().equals(advertisement.getId())){
                    throw new UserAlreadyReportedThisException();
                }
            }
        }

        reportDAO.addNewReport(reportDescription, StjUserMapper.toEntity(user), AdvertisementMapper.toEntity(advertisement));
    }
    
    public void deleteReportById(Integer reportId) {
        reportDAO.deleteReportById(reportId);
    }
    
    public ReportDto findReportById(Integer reportId) {
        return ReportMapper.toDto(reportDAO.findReportById(reportId));
    }
    
    public List<ReportDto> reportList(){
        List<ReportEntity> entityList = reportDAO.reportList();
        List<ReportDto> dtoList = new ArrayList<>();
        
        entityList.forEach((reportEntity) -> {
            dtoList.add(ReportMapper.toDto(reportEntity));
        });
        
        return dtoList;
    }
}
