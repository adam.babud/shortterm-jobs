
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.EducationDAO;
import com.bh10.project.shorttermjobs.dao.ExperienceDAO;
import com.bh10.project.shorttermjobs.dao.HardSkillDAO;
import com.bh10.project.shorttermjobs.dao.JobApplicantDataDAO;
import com.bh10.project.shorttermjobs.dao.LanguageDAO;
import com.bh10.project.shorttermjobs.dao.SoftSkillDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.dto.EducationDto;
import com.bh10.project.shorttermjobs.dto.ExperienceDto;
import com.bh10.project.shorttermjobs.dto.HardSkillDto;
import com.bh10.project.shorttermjobs.dto.JobApplicantDataDto;
import com.bh10.project.shorttermjobs.dto.LanguageDto;
import com.bh10.project.shorttermjobs.dto.SoftSkillDto;
import com.bh10.project.shorttermjobs.dto.StjUserDto;
import com.bh10.project.shorttermjobs.entity.EducationEntity;
import com.bh10.project.shorttermjobs.entity.ExperienceEntity;
import com.bh10.project.shorttermjobs.entity.HardSkillEntity;
import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import com.bh10.project.shorttermjobs.entity.LanguageEntity;
import com.bh10.project.shorttermjobs.entity.SoftSkillEntity;
import com.bh10.project.shorttermjobs.mapper.EducationMapper;
import com.bh10.project.shorttermjobs.mapper.ExperienceMapper;
import com.bh10.project.shorttermjobs.mapper.HardSkillMapper;
import com.bh10.project.shorttermjobs.mapper.JobApplicantDataMapper;
import com.bh10.project.shorttermjobs.mapper.LanguageMapper;
import com.bh10.project.shorttermjobs.mapper.SoftSkillMapper;
import com.bh10.project.shorttermjobs.mapper.StjUserMapper;
import java.security.Principal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class ProfileService {

    @Inject
    EducationDAO educationDAO;

    @Inject
    StjUserDAO stjUserDAO;

    @Inject
    ExperienceDAO experienceDAO;

    @Inject
    SoftSkillDAO softSkillDAO;

    @Inject
    HardSkillDAO hardSkillDAO;

    @Inject
    JobApplicantDataDAO jobApplicantDataDAO;

    @Inject
    LanguageDAO languageDAO;

    public ProfileService() {
    }

    public ProfileService(EducationDAO educationDAO, StjUserDAO stjUserDAO, ExperienceDAO experienceDAO, SoftSkillDAO softSkillDAO, HardSkillDAO hardSkillDAO, JobApplicantDataDAO jobApplicantDataDAO, LanguageDAO languageDAO) {
        this.educationDAO = educationDAO;
        this.stjUserDAO = stjUserDAO;
        this.experienceDAO = experienceDAO;
        this.softSkillDAO = softSkillDAO;
        this.hardSkillDAO = hardSkillDAO;
        this.jobApplicantDataDAO = jobApplicantDataDAO;
        this.languageDAO = languageDAO;
    }

    public Integer getUserTypeIdByPrincipal(Principal principal) {
        return stjUserDAO.getUserByEmail(principal.getName()).getUserType().getId();
    }

    public Integer getUserTypeIdByUserId(Integer id) {
        return stjUserDAO.getStjUserById(id).getUserType().getId();
    }

    public StjUserDto findUserByPrincipal(Principal principal) {
        return StjUserMapper.toDto(stjUserDAO.getUserByEmail(principal.getName()));
    }

    public StjUserDto findUserByEmail(String email) {
        return StjUserMapper.toDto(stjUserDAO.getUserByEmail(email));
    }

    public StjUserDto findUserById(Integer id) {
        return StjUserMapper.toDto(stjUserDAO.getStjUserById(id));
    }

    public List<EducationDto> findEducationByUserId(Integer id) {
        List<EducationEntity> list = educationDAO.findEducationByJobApplicant(jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id)));
        List<EducationDto> dtoList = new ArrayList<>();
        list.forEach((educationEntity) -> {
            dtoList.add(EducationMapper.toDto(educationEntity));
        });
        return dtoList;
    }

    public List<ExperienceDto> findExperienceByUserId(Integer id) {
        List<ExperienceEntity> list = experienceDAO.findExperienceByJobApplicant(jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id)));
        List<ExperienceDto> dtoList = new ArrayList<>();
        list.forEach((experienceEntity) -> {
            dtoList.add(ExperienceMapper.toDto(experienceEntity));
        });
        return dtoList;
    }

    public List<SoftSkillDto> findSofSkillById(Integer id) {
        List<SoftSkillEntity> list = softSkillDAO.findSoftSkillByJobApplicant(jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id)));
        List<SoftSkillDto> dtoList = new ArrayList<>();
        list.forEach((softSkillEntity) -> {
            dtoList.add(SoftSkillMapper.toDto(softSkillEntity));
        });
        return dtoList;
    }

    public List<HardSkillDto> findHardSkillByUserId(Integer id) {
        List<HardSkillEntity> list = hardSkillDAO.findHardSkillByJobApplicant(jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id)));
        List<HardSkillDto> dtoList = new ArrayList<>();
        list.forEach((hardSkillEntity) -> {
            dtoList.add(HardSkillMapper.toDto(hardSkillEntity));
        });
        return dtoList;
    }

    public List<LanguageDto> findLanguageByUserId(Integer id) {
        List<LanguageEntity> list = languageDAO.findLanguageByJobApplicant(jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id)));
        List<LanguageDto> dtoList = new ArrayList<>();
        list.forEach((languageEntity) -> {
            dtoList.add(LanguageMapper.toDto(languageEntity));
        });
        return dtoList;
    }

    public JobApplicantDataDto findDataByUserId(Integer id) {
        return JobApplicantDataMapper.toDto(jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id)));
    }

    public JobApplicantDataDto findDataByPrincipal(Principal principal) {
        return JobApplicantDataMapper.toDto(jobApplicantDataDAO.findDataByUser(StjUserMapper.toEntity(findUserByPrincipal(principal))));
    }

    public void updatePersonalDatas(Integer id, String address, String phoneNumber, String cvEmail, String cvName) {
        JobApplicantDataEntity entity = jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id));
        entity.setAddress(address);
        entity.setPhoneNumber(phoneNumber);
        entity.setCvEmail(cvEmail);
        entity.setCvName(cvName);
        jobApplicantDataDAO.updatePersonalDatas(entity);
    }

    public void updateImagePath(JobApplicantDataDto jobApplicantData) {
        jobApplicantDataDAO.updatePersonalDatas(JobApplicantDataMapper.toEntity(jobApplicantData));
    }

    public void updateHardSkillById(Integer id, String name, Integer level) {
        HardSkillEntity entity = hardSkillDAO.findHardSkillById(id);
        entity.setName(name);
        entity.setLevel(level);
        hardSkillDAO.updateHardSkill(entity);
    }

    public void createNewHardSkillByJobApplicant(Integer id, String name, Integer level) {
        JobApplicantDataEntity entity = jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id));
        HardSkillEntity hardSkillEntity = new HardSkillEntity();
        hardSkillEntity.setJobApplicantData(entity);
        hardSkillEntity.setName(name);
        hardSkillEntity.setLevel(level);
        hardSkillDAO.addNewHardSkill(hardSkillEntity);
    }

    public void updateSoftSkillById(Integer id, String name, Integer level) {
        SoftSkillEntity entity = softSkillDAO.findSoftSkillById(id);
        entity.setName(name);
        entity.setLevel(level);
        softSkillDAO.updateSoftSkill(entity);
    }

    public void createNewSoftSkillByJobApplicant(Integer id, String name, Integer level) {
        JobApplicantDataEntity entity = jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id));
        SoftSkillEntity softSkillEntity = new SoftSkillEntity();
        softSkillEntity.setJobApplicantData(entity);
        softSkillEntity.setName(name);
        softSkillEntity.setLevel(level);
        softSkillDAO.addNewSoftSkill(softSkillEntity);
    }

    public void updateLanguageById(Integer id, String name, String level) {
        LanguageEntity entity = languageDAO.findLanguageById(id);
        entity.setName(name);
        entity.setLevel(level);
        languageDAO.updateLanguage(entity);
    }

    public void createNewLanguageByJobApplicant(Integer id, String name, String level) {
        JobApplicantDataEntity entity = jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id));
        LanguageEntity languageEntity = new LanguageEntity();
        languageEntity.setJobApplicantData(entity);
        languageEntity.setName(name);
        languageEntity.setLevel(level);
        languageDAO.addNewLanguage(languageEntity);
    }

    public void updateExperienceById(Integer id, String company, String term, String position, String description) {
        ExperienceEntity entity = experienceDAO.findExperienceById(id);
        entity.setCompany(company);
        entity.setTerm(term);
        entity.setPosition(position);
        entity.setDescription(description);
        experienceDAO.updateExperience(entity);
    }

    public void createNewExperience(Integer id, String company, String term, String position, String description) {
        JobApplicantDataEntity entity = jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id));
        ExperienceEntity experienceEntity = new ExperienceEntity();
        experienceEntity.setJobApplicantData(entity);
        experienceEntity.setCompany(company);
        experienceEntity.setTerm(term);
        experienceEntity.setPosition(position);
        experienceEntity.setDescription(description);
        experienceDAO.addNewExperience(experienceEntity);
    }

    public void updateEducationById(Integer id, String school, Date startDate, Date endDate, String qualification) {
        EducationEntity entity = educationDAO.findEducationById(id);
        entity.setSchool(school);
        entity.setStartDate(startDate);
        entity.setEndDate(endDate);
        entity.setQualification(qualification);
        educationDAO.updateEducation(entity);
    }

    public void createNewEducation(Integer id, String school, Date startDate, Date endDate, String qualification) {
        JobApplicantDataEntity jobApplicantDataEntity = jobApplicantDataDAO.findDataByUser(stjUserDAO.getStjUserById(id));
        EducationEntity entity = new EducationEntity();
        entity.setJobApplicantData(jobApplicantDataEntity);
        entity.setSchool(school);
        entity.setStartDate(startDate);
        entity.setEndDate(endDate);
        entity.setQualification(qualification);
        educationDAO.addNewEducation(entity);
    }

    public void deleteHardSkill(Integer id) {
        hardSkillDAO.deleteHardSKill(hardSkillDAO.findHardSkillById(id));
    }

    public void deleteSoftSkill(Integer id) {
        softSkillDAO.deleteSoftSkill(softSkillDAO.findSoftSkillById(id));
    }

    public void deleteLanguage(Integer id) {
        languageDAO.deleteLanguage(languageDAO.findLanguageById(id));
    }

    public void deleteExperience(Integer id) {
        experienceDAO.deleteExperience(experienceDAO.findExperienceById(id));
    }

    public void deleteEducation(Integer id) {
        educationDAO.deleteEducation(educationDAO.findEducationById(id));
    }

}
