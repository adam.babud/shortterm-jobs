
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.EmployerDataDAO;
import com.bh10.project.shorttermjobs.dao.JobApplicantDataDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.dto.StjUserDto;
import com.bh10.project.shorttermjobs.entity.EmployerDataEntity;
import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import com.bh10.project.shorttermjobs.entity.UserTypeEntity;
import com.bh10.project.shorttermjobs.exception.EmailIsUsedException;
import com.bh10.project.shorttermjobs.mapper.StjUserMapper;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class StjUserService {
    
    @Inject
    StjUserDAO stjUserDAO;
    
    @Inject
    GroupService groupService;
    
    @Inject
    UserTypeService userTypeService;
    
    @Inject
    JobApplicantDataDAO jobApplicantDataDAO;
    
    @Inject    
    EmployerDataDAO employerDataDAO;
    
    public StjUserService(StjUserDAO stjUserDAO, GroupService groupService, UserTypeService userTypeService, JobApplicantDataDAO jobApplicantDataDAO, EmployerDataDAO employerDataDAO) {
        this.stjUserDAO = stjUserDAO;
        this.groupService = groupService;
        this.userTypeService = userTypeService;
        this.jobApplicantDataDAO = jobApplicantDataDAO;
        this.employerDataDAO = employerDataDAO;
    }

    public StjUserService() {
    }
    
    
    
    public void createNewUser(String email, String name, String password, UserTypeEntity userTypeEntity)
            throws EmailIsUsedException, UnsupportedEncodingException, NoSuchAlgorithmException {
        
        if (stjUserDAO.getCountOfUsersByEmail(email) != 0) {
            throw new EmailIsUsedException();
        }        
        stjUserDAO.createNewUser(email, name, encodeSHA256(password), userTypeEntity, groupService.findUserGroup());
        if (userTypeEntity.getId() == 2) {
            loadDeafultPrivateUserData(stjUserDAO.getUserByEmail(email));
        } else {
            loadDefaultEmployerData(stjUserDAO.getUserByEmail(email));
        }
    }
    
    public void createNewAdmin(String email, String name, String password)
            throws EmailIsUsedException, UnsupportedEncodingException, NoSuchAlgorithmException {
        
        if (stjUserDAO.getCountOfUsersByEmail(email) != 0) {
            throw new EmailIsUsedException();
        }
        stjUserDAO.createNewUser(email, name, encodeSHA256(password), userTypeService.findUserTypeByName("admin"), groupService.findAdminGroup());        
    }
    
    public static String encodeSHA256(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes(StandardCharsets.UTF_8));
        byte[] digest = md.digest();
        
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < digest.length; i++) {
            String hex = Integer.toHexString(0xff & digest[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        
        return hexString.toString();
        
    }
    
    public StjUserDto getUserByEmail(String userEmail) {
        return StjUserMapper.toDto(stjUserDAO.getUserByEmail(userEmail));
    }    
    
    public void loadDeafultPrivateUserData(StjUserEntity entity) {
        JobApplicantDataEntity jobApplicantDataEntity = new JobApplicantDataEntity();
        jobApplicantDataEntity.setUserId(entity);
        jobApplicantDataDAO.persistData(jobApplicantDataEntity);
    }
    
    public void loadDefaultEmployerData(StjUserEntity entity) {
        EmployerDataEntity employerDataEntity = new EmployerDataEntity();
        employerDataEntity.setUser(entity);
        employerDataDAO.createData(employerDataEntity);
    }

    
 
    
}
