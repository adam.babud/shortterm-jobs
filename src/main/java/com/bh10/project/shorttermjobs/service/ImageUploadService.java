package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.JobApplicantDataDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class ImageUploadService {

    @Inject
    private JobApplicantDataDAO jobApplicantDataDao;
    
    @Inject 
    private StjUserDAO stjUserDAO;

    public ImageUploadService() {
    }

    public ImageUploadService(JobApplicantDataDAO jobApplicantDataDao, StjUserDAO stjUserDAO) {
        this.jobApplicantDataDao = jobApplicantDataDao;
        this.stjUserDAO = stjUserDAO;
    }
    
    public void persistProfileImagePath(String userEmail, String profImagePath) {
        StjUserEntity userDto = stjUserDAO.getUserByEmail(userEmail);
        JobApplicantDataEntity personalData = jobApplicantDataDao.findDataByUser(userDto);
        
        personalData.setProfImgPath(profImagePath);
        
        jobApplicantDataDao.updatePersonalDatas(personalData);
    }
}
