
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.UserTypeDAO;
import com.bh10.project.shorttermjobs.entity.UserTypeEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class UserTypeService {
    
    @Inject
    UserTypeDAO userTypeDAO;

    public UserTypeService() {
    }

    public UserTypeService(UserTypeDAO userTypeDAO) {
        this.userTypeDAO = userTypeDAO;
    }
    
    
    
    public UserTypeEntity findUserTypeByName(String name){
        return userTypeDAO.findUserTypeByName(name);
    }
    
    public List<UserTypeEntity> listUserType(){
        return userTypeDAO.listUserType();
    }
    
}
