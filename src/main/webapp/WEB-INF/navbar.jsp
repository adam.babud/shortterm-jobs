
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Short-Term Jobs</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/navbar.css"/>" >
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/navbar_query.css"/>" >
    </head>
    <body class="nav-body">
        <a class="nav-top-anchor" href="#"><img onclick="navTop()" id="scroll-top" class="nav-top-icon" src="<c:url value="resources/images/icons/nav_top_icon.png"/>" alt="Nav Top Icon"></a>

        <header class="head">
            <div class="logo-container">
                <img src="<c:url value="resources/images/logo.jpg"/>" alt="Company Logo">
                <p>SHORT - TERM JOBS</p>
            </div>
            <div class="toggle">
                <img id="menu_icon" class="nav_icon" onclick="dropDownMenu()" style="color: white;"src="<c:url value="resources/images/icons/mobile_nav_icon.png"/>" alt="Nav Icon">
            </div>
            <c:if test="${!pageContext.request.isUserInRole('user') && !pageContext.request.isUserInRole('admin')}">
                <table id="navbar" class="menu">
                    <th><a href="advertisements">Advertisements</a></th>
                    <th><a href="login.jsp">Login</a></th>
                    <th><a href="registration">Registration</a></th>
                </table>
            </c:if>
            <c:if test="${pageContext.request.isUserInRole('admin')}">
                <table id="navbar">
                    <th><a href="advertisements">Advertisements</a></th>
                    <th><a href="createAdmin">Create Admin Account</a></th>
                    <th><a href="reports">Reports</a></th>
                    <th><a href="logout">Logout</a></th>
                </table>
            </c:if>
            <c:if test="${pageContext.request.isUserInRole('user')}">
                <table id="navbar">
                    <th><a href="advertisements">Advertisements</a></th>
                    <th><a href="createadvertisement">Create Advertisement</a></th>
                    <th><a href="profile?query=myprofile">My Profile</a></th>
                    <th><a href="myAdvertisements">My Advertisements</a></th>
                    <th><a href="checkmyapplications">My Applications</a></th>
                    <th><a href="logout">Logout</a></th>
                </table>
            </c:if>
        </header>
    </body>
    <script type="text/javascript" src="<c:url value="resources/js/mobilenav.js"/>"></script>
</html>
