
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
    <head>
        <title>Company Profile</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/employerProfile.css"/>" />
    </head>
    <body class="company-profile-body">
        <jsp:include page="navbar.jsp"></jsp:include>
            <div class="edit-profile">
                <a href="editprofile"><img src="<c:url value="resources/images/icons/edit_icon.png"/>" alt="edit_icon"></a>
            </div>
            <div class="profile-column1">
                <div id="company-data" class="company-data">
                    <div class="pd-header">
                        <h3>Company Data</h3>
                    </div>
                    <p class="user-name">${company.name}</p>
                <div class="prof-home-address">
                    <img src="<c:url value="resources/images/icons/house_icon.png"/>" alt="house_icon"><p>${company.address}</p>
                </div>
                <div class="prof-email">
                    <img src="<c:url value="resources/images/icons/email_icon.png"/>" alt="email_icon"><p>${company.email}</p>
                </div>
                <div class="prof-phone">
                    <img src="<c:url value="resources/images/icons/phone_icon.png"/>" alt="phone_icon"><p>${company.phoneNumber}</p>
                </div>
                <div class="prof-hr-contact">
                    <img src="<c:url value="resources/images/icons/contact_icon.png"/>" alt="contact_icon"><p>${company.hrContact}</p>
                </div>
                <div class="prof-size">
                    <p>Company size: ${company.size}</p>
                </div>

            </div>
        </div>


        <div class="profile-column2">
            <div class="business-scope">
                <div class="business-scope-header"><img src="<c:url value="resources/images/icons/briefcase_icon.png"/>" alt="briefcase_icon"><h3>Business Scope</h3></div>
                <p>${company.businessScope}</p>
            </div>
            <div class="business-description">
                <div class="business-desc-header"><h3>Company Description</h3></div>
                <p>${company.description}</p>
            </div>
        </div>


    </body>
</html>

