var catCollapseBtn = document.getElementById("cat-collapse-btn");
var catExpandBtn = document.getElementById("cat-expand-btn");
var locCollapseBtn = document.getElementById("loc-collapse-btn");
var locExpandBtn = document.getElementById("loc-expand-btn");

function collapseCategory() {
    hideCatTableRows();
    catCollapseBtn.style.display = "none";
    catExpandBtn.style.display = "inline-block";
}
function collapseLocation() {
    hideLocTableRows();
    locCollapseBtn.style.display = "none";
    locExpandBtn.style.display = "inline-block";

}
function expandCategory() {
    showCatTableRows();
    catCollapseBtn.style.display = "inline-block";
    catExpandBtn.style.display = "none";
}
function expandLocation() {
    showLocTableRows();
    locCollapseBtn.style.display = "inline-block";
    locExpandBtn.style.display = "none";
}

function hideCatTableRows() {
    document.getElementById("cat-table-rows").style.display = "none";
}

function showCatTableRows() {
    document.getElementById("cat-table-rows").style.display = "block";
}

function hideLocTableRows() {
    document.getElementById("loc-table-rows").style.display = "none";
}

function showLocTableRows() {
    document.getElementById("loc-table-rows").style.display = "block";
}


