loadSkillLevelBars();
loadLanguageLevelBars();
function loadSkillLevelBars() {
    var skillLevels = document.getElementsByClassName("skill-level");
    var skillBars = document.getElementsByClassName("current-skill-level");
    for (var i = 0; i < skillLevels.length; i++) {
        if (skillLevels[i] > 100) {
           skillBars[i].style.width = "100%";
       } else {
           skillBars[i].style.width = skillLevels[i].textContent + "%";
       }
    }
}
function loadLanguageLevelBars() {
    var langLevels = document.getElementsByClassName("lang-level");
    var langLevelBars = document.getElementsByClassName("current-lang-level");

    for (var i = 0; i < langLevels.length; i++) {
        var level = langLevels[i].textContent;
        var barWidth;
        switch (level) {
            case "A1":
                barWidth = 16.66;
                break;
            case "A2":
                barWidth = 33.32;
                break;
            case "B1":
                barWidth = 50;
                break;
            case "B2":
                barWidth = 66.66;
                break;
            case "C1":
                barWidth = 83.32;
                break;
            case "C2":
                barWidth = 100;
                break;
            default:
                0;
        }
        langLevelBars[i].style.width = barWidth + "%";
    }
}


