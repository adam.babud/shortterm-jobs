function editProfilePicture() {
    var editPicContainer = document.getElementById("edit-profile-pic-container");
    editPicContainer.style.display = "inline-block";
}


function editPersonalData() {
    var personalData = document.getElementById("personal_data");
    var editPersonalDataContainer = document.getElementById("edit-personal-data");
    
    personalData.style.display = "none";
    editPersonalDataContainer.style.display = "inline-block"; 
}
function editSkills() {
    var skillContainer = document.getElementById("skills-container");
    var editSkillContainer = document.getElementById("edit-skills-container");
    
    skillContainer.style.display = "none";
    editSkillContainer.style.display = "inline-block"; 
}

function addSkill() {
    var addSkillContainer = document.getElementById("add-skills-container");
    var addSkillButton = document.getElementById("add-skill-btn");
    addSkillContainer.style.display = "inline-block";
    addSkillButton.style.display = "none";
}


function editLanguages() {
    var langContainer = document.getElementById("lang-container");
    var editLangContainer = document.getElementById("edit-languages-container");
    
    langContainer.style.display = "none";
    editLangContainer.style.display = "inline-block"; 
}

function addLanguage() {
    var addLangContainer = document.getElementById("add-language-container");
    var addLangButton = document.getElementById("add-lang-btn");
    addLangContainer.style.display = "inline-block";
    addLangButton.style.display = "none";
}
function editWorkExp() {
    var workExpContainer = document.getElementById("work-exp-container");
    var editWorkExpContainer = document.getElementById("edit-workexp-container");
    
    workExpContainer.style.display = "none";
    editWorkExpContainer.style.display = "inline-block"; 
}
//nincs kész
function addWorkExp() {
    var addWorkExpContainer = document.getElementById("add-workexp-container");
    var addWorkExpButton = document.getElementById("add-workexp-btn");
    addWorkExpContainer.style.display = "inline-block";
    addWorkExpButton.style.display = "none";
}

function editEducation() {
    var educContainer = document.getElementById("education-container");
    var editEducContainer = document.getElementById("edit-education-container");
    
    educContainer.style.display = "none";
    editEducContainer.style.display = "inline-block"; 
}
//nincs kész
function addEducation() {
    var addEduContainer = document.getElementById("add-education-container");
    var addEduButton = document.getElementById("add-education-btn");
    addEduContainer.style.display = "inline-block";
    addEduButton.style.display = "none";
}


