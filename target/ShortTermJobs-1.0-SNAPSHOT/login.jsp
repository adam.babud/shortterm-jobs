
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/login.css" />" >
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/login_query.css" />" >
        <title>Login</title>
    </head>
    <body class="login-body">
        <jsp:include page="/WEB-INF/navbar.jsp"/>
        <div class="login_container">
            <form action="j_security_check" method="post">
                <div class="imgcontainer">
                    <img class="avatar" src="<c:url value="resources/images/login_avatar.jpg"/>" alt="Avatar">
                </div>
                <div class="container">
                    <label for="email"><b>Email</b></label>
                    <input type="text" placeholder="Enter Email" name="j_username" required>

                    <label for="password"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="j_password" required>

                    <button type="submit">Login</button>
                    <label class="check-box">
                        <input type="checkbox" checked="checked" name="remember"> Remember me
                    </label>
                </div>
                <div class="container-cnc-psw" style="background-color:#f1f1f1">
                    <button type="button" class="cancelbtn">Cancel</button>
                    <span class="psw">Forgot <a href="#">password?</a></span>
                </div>
            </form>
        </div>
    </body>
</html>
