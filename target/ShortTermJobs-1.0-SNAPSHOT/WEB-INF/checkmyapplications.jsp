
<%@page import="com.bh10.project.shorttermjobs.dto.StjUserDto"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/advertisement.css"/>" />
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/advertisement_query.css"/>"/>
        <title>My Applications</title>
    </head>
    <body class="advertisement-body">
        <jsp:include page="navbar.jsp"></jsp:include>
        <c:if test="${userHasNoApplication}">
            <h2>You have no applications yet!</h2>
        </c:if>
        <c:if test="${!userHasNoApplication}">
            <div class="advertisement-container">
                <div class="advert-container">
                    <div class="my-app-header">
                        <h2>My Applications</h2>
                    </div>
                    <c:forEach items="${advertisements}" var="item">
                        <div class="advert">
                            <div class="jobtype">
                                <img class="search-icon" src="<c:url value="resources/images/icons/job_icon.png"/>" alt="Job Icon"><p>${item.jobType}</p>
                            </div>
                            <div class="location">
                                <img class="search-icon" src="<c:url value="resources/images/icons/location_icon.png"/>" alt="Location Icon"><p>${item.location}</p>
                            </div>
                            <div class="date">
                                <img class="search-icon" src="<c:url value="resources/images/icons/calendar_icon.png"/>" alt="Calendar Icon"><p>${item.date}</p>
                            </div>
                            <div class="contact">
                                <img src="<c:url value="resources/images/icons/contact_icon.png"/>" alt="Contact Icon">
                                <ul>
                                    <li>${item.stjUserDto.name}</li>
                                    <li>${item.stjUserDto.email}</li>
                                </ul>
                            </div>
                            <a href="profile?query=${item.stjUserDto.id}">Check Advertiser Profile</a>
                            <div class="report-icon">
                                <a style="" href="reportAdvertisement?query=${item.id}"><img src="<c:url value="resources/images/icons/report_icon.png"/>" alt="report_icon"></a>
                                <p class="report-txt">Report advertisement</p>
                            </div>
                            <div class="description">
                                <p>${item.description}</p>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </c:if>    
</body>
<script type="text/javascript" src="<c:url value="resources/js/advertisement.js"/>"></script>

</html>
