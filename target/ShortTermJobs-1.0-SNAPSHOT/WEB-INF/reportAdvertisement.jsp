
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/advertisement.css"/>" />
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/reportAdvertisement.css"/>" />
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/advertisement_query.css"/>"/>
        <title>Report</title>
    </head>
    <body>
        <jsp:include page="navbar.jsp"></jsp:include>
            <div class="advert-container">
                <div class="advert">
                    <div class="jobtype">
                        <img class="search-icon" src="<c:url value="resources/images/icons/job_icon.png"/>" alt="Job Icon"><p>${advertisement.getJobType()}</p>
                </div>
                <div class="location">
                    <img class="search-icon" src="<c:url value="resources/images/icons/location_icon.png"/>" alt="Location Icon"><p>${advertisement.getLocation()}</p>
                </div>
                <div class="date">
                    <img class="search-icon" src="<c:url value="resources/images/icons/calendar_icon.png"/>" alt="Calendar Icon"><p>${advertisement.getDate()}</p>
                </div>
                <div class="contact">
                    <img src="<c:url value="resources/images/icons/contact_icon.png"/>" alt="Contact Icon">
                    <ul>
                        <li>${advertisement.stjUserDto.name}</li>
                        <li>${advertisement.stjUserDto.email}</li>
                    </ul>
                </div>
                <div class="report-container">
                    <form action="reportAdvertisement" method="post">
                        <label for="descriptionOfReport"><b>Please provide a description</b></label>
                        <input type="text" placeholder="Please provide a description" name="descriptionOfReport" required>
                        <button type="submit">Report</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>