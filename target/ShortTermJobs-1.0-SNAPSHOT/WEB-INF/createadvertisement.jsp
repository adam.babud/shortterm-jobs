
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Creat Advertisement</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/createAdvertisement.css"/>" />
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/advertisement_query.css"/>"/>
    </head>
    <body class="create-advert-body">
        <jsp:include page="navbar.jsp"></jsp:include>
            <form action="createadvertisement" method="post">
                <div class="advert-container">
                    <div class="advert">
                    <c:if test="${dateFormat == true}">
                        <h5 class="error-msg">Wrong date format!</h5>
                    </c:if>
                    <div class="jobtype">
                        <img src="<c:url value="resources/images/icons/job_icon.png"/>" alt="Job Icon">
                        <input type="text" name="jobType" placeholder="Enter JobType" required="true">
                    </div>
                    <div class="location">
                        <img src="<c:url value="resources/images/icons/location_icon.png"/>" alt="Location Icon">
                        <input type="text" name="location" placeholder="Enter Location" required="true">
                    </div>
                    <div class="date">
                        <img src="<c:url value="resources/images/icons/calendar_icon.png"/>" alt="Calendar Icon">
                        <form>
                            <input type="date" name="date" placeholder="Enter Date" required="true">
                        </form>
                    </div>
                    <div class="jobtype">
                        <img src="<c:url value="resources/images/icons/category_icon.png"/>" alt="category_icon">
                        <input type="text" name="category" placeholder="Enter Category" required="true">
                    </div>
                    <div class="contact">
                        <img src="<c:url value="resources/images/icons/contact_icon.png"/>" alt="Contact Icon">
                        <ul>
                            <li>${user.name}</li>
                            <li>${user.email}</li>
                        </ul>
                    </div>
                    <div class="description">
                        <label for="description">Description:</label>
                        <input type="text" name="description" placeholder="Enter Description">
                    </div>
                    <button type="submit">Create</button>
                </div>
        </form>

    </body>
</html>
