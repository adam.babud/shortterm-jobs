<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/registration.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/registration_query.css"/>"/>
    </head>
    <body class="registration-body">
        <jsp:include page="navbar.jsp"></jsp:include>
            <div class="registration_container">
            <c:if test = "${registeredSuccessfully}">
                <h4 class="success-msg">Registration was successful!</h4>
            </c:if>
            <c:if test = "${emailIsUsed}">
                <h4 class="error-msg">Email is used!</h4>
            </c:if>
            <div class="header">
                <h1>REGISTRATION</h1>
            </div>
            <div class="container">
                <form action="registration" method="post">
                    <label for="email"><b>Email address</b></label>
                    <input type="text" placeholder="Enter email address" name="email" required>

                    <label for="password"><b>Password</b></label>
                    <input type="password" placeholder="Enter password" name="password" required>

                    <label for="name"><b>Name</b></label>
                    <input type="text" placeholder="End name" name="name" required>

                    <label><b>Select user type</b></label> 
                    <select name="userTypeName">
                        <c:forEach items="${userTypeList}" var="userType">
                            <option value="${userType.getName()}">${userType.getName()}</option>
                        </c:forEach>
                    </select>
                    <button type="submit">Register</button>
                </form>
            </div>

        </div>
    </body>
</html>
