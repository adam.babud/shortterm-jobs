
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Profile</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/profile.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/profile_query.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/editprofile.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/editprofile_query.css"/>"/>
    </head>
    <body class="profile-body">
        <jsp:include page="navbar.jsp"></jsp:include>
            <div class="profile-column1">
                <div class="prof-img-data-container">
                    <div class="profile-picture">
                    <c:if test="${not empty data.profImgPath}">
                        <img class="prof-img" src="displayImage?name=${data.profImgPath}" alt="profile_picture">
                    </c:if>
                    <c:if test="${empty data.profImgPath}">
                        <img class="prof-img" src="<c:url value="resources/images/login_avatar.jpg"/>" alt="profile_picture">
                    </c:if>
                        <button onclick="editProfilePicture()" class="edit-pic-btn"><img src="<c:url value="resources/images/icons/edit_icon.png"/>" alt="edit_btn"></button>
                </div>
                <!--HIDDEN EDIT PROFILE PIC-->
                <div id="edit-profile-pic-container">
                    <form action="uploadProfile" method="POST" enctype="multipart/form-data">
                        <input type="file" name="file" size="50"/>
                        <button type="submit"><img src="<c:url value="resources/images/icons/save_icon.png"/>" alt="save_icon"></button>
                    </form>
                </div>
                <div id="personal_data" class="personal-data">
                    <div class="pd-header">
                        <h3>Personal Data</h3>
                    </div>
                    <p class="user-name">${data.cvName}</p>
                    <div class="prof-home-address">
                        <img src="<c:url value="resources/images/icons/house_icon.png"/>" alt="house_icon"><p>${data.address}</p>
                    </div>
                    <div class="prof-email">
                        <img src="<c:url value="resources/images/icons/email_icon.png"/>" alt="email_icon"><p>${data.cvEmail}</p>
                    </div>
                    <div class="prof-phone">
                        <img src="<c:url value="resources/images/icons/phone_icon.png"/>" alt="phone_icon"><p>${data.phoneNumber}</p>
                    </div>
                    <div class="edit-pdata">
                        <button onclick="editPersonalData()" class="edit-pdata-btn"><img src="<c:url value="resources/images/icons/edit_icon.png"/>"></button>
                    </div>
                </div>
                <!--********hidden personal data form***********/-->
                <div id="edit-personal-data">
                    <form action="editprofile?query=datas" method="post">
                        <div class="pd-header">
                            <h3>Personal Data</h3>
                        </div>
                        <div class="pd-name">
                            <label for="cvName"><b>Name</b></label>
                            <input type="text" name="cvName" value="${data.cvName}">
                        </div>
                        <div class="pd-email">
                            <label for="cvEmail"><b>Email</b></label>
                            <input type="text" name="cvEmail" value="${data.cvEmail}" >
                        </div>
                        <div class="pd-address">
                            <label for="address"><b>Address</b></label>
                            <input type="text" name="address" value="${data.address}">
                        </div>
                        <div class="pd-phone">
                            <label for="phoneNumber"><b>Phone</b></label>
                            <input type="text" name="phoneNumber" value="${data.phoneNumber}" >
                        </div>
                        <div class="save-btn">
                            <button type="submit"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                        </div>
                    </form>
                </div>
            </div>
            <div id="work-exp-container" class="work-experience">
                <div class="workexp-header"><img src="<c:url value="resources/images/icons/briefcase_icon.png"/>" alt="briefcase_icon"><h3>Work Experience</h3></div>
                    <c:forEach items="${experiences}" var="item">
                    <div class="work-exp">
                        <p class="job-position">${item.position}</p>
                        <p class="term">${item.term}</p>
                        <p class="company-name">${item.company}</p>
                        <div class="description">
                            <p>${item.description}</p>
                        </div>
                    </div>
                </c:forEach>
                <div class="work-btns">
                    <div id="add-workexp-btn" class="add-workexp">
                        <button onclick="addWorkExp()" class="add-wexp-btn"><img src="<c:url value="resources/images/icons/add_icon.png"/>"></button>
                    </div>
                    <div class="edit-workexp">
                        <button onclick="editWorkExp()" class="edit-wexp"><img src="<c:url value="resources/images/icons/edit_icon.png"/>"></button>
                    </div>
                </div>
                <!--HIDDEN ADD WORK EXP CONTAINER-->
                <div id="add-workexp-container">
                    <form action="editprofile?query=addExperince" method="post">
                        <label for="company"><b>Company</b></label>
                        <input type="text" name="newCompany" placeholder="Enter Company" required="true">
                        <label for="term"><b>Term</b></label>
                        <input type="text" name="newTerm" placeholder="Enter Term" required="true">
                        <label for="position"><b>Position</b></label>
                        <input type="text" name="newPosition" placeholder="Enter Position" required="true">
                        <label class="edit-desc-label" for="description"><b>Description</b></label>
                        <input class="edit-desc-input" type="text" name="newDescription" placeholder="Write Description">
                        <div class="add-btn">
                            <button type="submit"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                        </div>
                        <br>
                    </form>
                </div>
            </div>


            <!--HIDDEN EDIT WORKEXP CONTAINER-->
            <div id="edit-workexp-container">
                <div class="workexp-header"><img src="<c:url value="resources/images/icons/briefcase_icon.png"/>" alt="briefcase_icon"><h3>Work Experience</h3></div>
                    <c:forEach items="${experience}" var="item">
                    <form action="editprofile?query=updateExperience" method="post">
                        <label for="company"><b>Company</b></label>
                        <input type="text" name="updateCompany" value="${item.company}" required="true">
                        <label for="term"><b>Term</b></label>
                        <input type="text" name="updateTerm" value="${item.term}" required="true">
                        <label for="position"><b>Position</b></label>
                        <input type="text" name="updatePosition" value="${item.position}" required="true">
                        <label class="edit-desc-label" for="description"><b>Description</b></label>
                        <input class="edit-desc-input" type="text" name="updateDescription" value="${item.description}">
                        <input type="hidden" name="experienceId" value="${item.id}">
                        <div class="save-delete-btns">
                            <div class="save-btn">
                                <button type="submit" name="workSubmit" value="update"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                            </div>
                            <div class="delete-btn">
                                <button type="submit" name="workSubmit" value="delete"><img src="<c:url value="resources/images/icons/delete_icon.png"/>" alt="delete_icon"></button>
                            </div>
                        </div>
                        <br>
                    </form>
                </c:forEach>
            </div>
            <!--end of hidden edit workexp-->

            <div id="education-container" class="education">
                <div class="ed-header"><img src="<c:url value="resources/images/icons/education_icon.png"/>" alt="education_icon"><h3>Education</h3></div>
                    <c:forEach items="${educations}" var="item">
                    <div class="educ-unit">
                        <p class="school-name">${item.school}</p>
                        <p class="date">from ${item.startDate} until ${item.endDate}</p>
                        <p class="qualification">${item.qualification}</p>
                    </div>
                </c:forEach>
                <div class="educ-btns">
                    <div id="add-education-btn" class="add-education">
                        <button onclick="addEducation()" class="add-edu-btn"><img src="<c:url value="resources/images/icons/add_icon.png"/>"></button>
                    </div>
                    <div class="edit-education">
                        <button onclick="editEducation()" class="edit-educ"><img src="<c:url value="resources/images/icons/edit_icon.png"/>"></button>
                    </div>
                </div>

                <!--HIDDEN ADD EDU CONTAINER-->
                <div id="add-education-container">
                    <form action="editprofile?query=addEducation" method="post">
                        <label for="school"><b>School</b></label>
                        <input type="text" name="newSchool" placeholder="Enter School" required="true">
                        <label for="startdate"><b>Start Date</b></label>
                        <input type="text" name="newStartDate" placeholder="Enter Start Date" required="true">
                        <label for="enddate"><b>End Date</b></label>
                        <input type="text" name="newEndDate" placeholder="Enter End Date" required="true">
                        <label for="qualification"><b>Qualification</b></label>
                        <input type="text" name="newQualification" placeholder="Enter Qualification" required="true">
                        <div class="add-btn">
                            <button type="submit"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                        </div>
                        <br>
                    </form>
                </div>
            </div>


            <!--HIDDEN EDIT EDUCATION CONTAINER-->
            <div id="edit-education-container">
                <div class="ed-header"><img src="<c:url value="resources/images/icons/education_icon.png"/>" alt="education_icon"><h3>Education</h3></div>
                    <c:forEach items="${education}" var="item">
                    <form action="editprofile?query=updateEducation" method="post">
                        <label for="school"><b>School</b></label>
                        <input type="text" name="updateSchool" value="${item.school}" required="true">
                        <label for="startdate"><b>Start Date</b></label>
                        <input type="text" name="updateStartDate" value="${item.startDate}" required="true">
                        <label for="enddate"><b>End Date</b></label>
                        <input type="text" name="updateEndDate" value="${item.endDate}" required="true">
                        <label for="qualification"><b>Qualification</b></label>
                        <input type="text" name="updateQualification" value="${item.qualification}" required="true">
                        <input type="hidden" name="educationId" value="${item.id}">
                        <div class="save-delete-btns">
                            <div class="save-btn">
                                <button type="submit" name="eduSubmit" value="update"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                            </div>
                            <div class="delete-btn">
                                <button type="submit" name="eduSubmit" value="delete"><img src="<c:url value="resources/images/icons/delete_icon.png"/>" alt="delete_icon"></button>
                            </div>
                        </div>
                        <br>
                    </form>                
                </c:forEach>
            </div>
            <!--end of hidden education container-->
        </div>




        <div class="profile-column2">
            <div id="skills-container" class="skills">
                <div class="edit-skills">
                    <button onclick="editSkills()" class="edit-skill"><img src="<c:url value="resources/images/icons/edit_icon.png"/>"></button>
                </div>
                <div class="sk-header"><img src="<c:url value="resources/images/icons/skill_icon.png"/>" alt="skill_icon"><h3>Skills</h3></div>
                    <c:forEach items="${hard}" var="item">
                    <div class="skill">
                        <p class="skill-name">${item.name}</p>
                        <p>Level: <span class="skill-level">${item.level}</span>%</p>
                        <div class="skill-lvl-bar"><h5 class="current-skill-level"></h5></div>
                    </div>
                </c:forEach>

                <c:forEach items="${soft}" var="item">
                    <div class="skill">
                        <p class="skill-name">${item.name}</p>
                        <p class="skill-lvl">Level: <span class="skill-level">${item.level}</span>%</p>
                        <div class="skill-lvl-bar"><h5 class="current-skill-level"></h5></div>
                    </div>
                </c:forEach>
                <div id="add-skill-btn" class="add-skill">
                    <button onclick="addSkill()" class="add-skill-btn"><img src="<c:url value="resources/images/icons/add_icon.png"/>"></button>
                </div>
                <!--HIDDEN ADD SKILLS CONTAINER-->
                <div id="add-skills-container">
                    <form action="editprofile?query=addHardSkills" method="post">
                        <label for="newName"><b>Hard Skill Name</b></label>
                        <input type="text" placeholder="Enter hard skill name" name="newName" required="true">
                        <label for="newLevel"><b>Level</b></label>
                        <input type="text" placeholder="Enter level (%)" name="newLevel" required="true">
                        <div class="add-btn">
                            <button type="submit"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                        </div>
                        <br>
                    </form>
                    <form action="editprofile?query=addSoftSkill" method="post">
                        <label for="newName"><b>Soft Skill Name</b></label>
                        <input type="text" placeholder="Enter soft skill name" name="newName" required="true">
                        <label for="newLevel"><b>Level</b></label>
                        <input type="text" placeholder="Enter level (%)" name="newLevel" required="true">
                        <div class="add-btn">
                            <button type="submit"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                        </div>
                        <br>
                    </form>
                </div>
                <!--END OF ADD SKILLS CONTAINER-->
            </div>


            <!--HIDDEN EDIT SKILLS CONTAINER-->
            <div id="edit-skills-container">
                <div class="sk-header"><img src="<c:url value="resources/images/icons/skill_icon.png"/>" alt="skill_icon"><h3>Skills</h3></div>
                    <c:forEach items="${hardSkills}" var="item">    
                    <form action="editprofile?query=updateHardSkills" method="post">
                        <label for="updateName"><b>Hard Skill Name</b></label>
                        <input type="text" name="updateName" value="${item.name}" required="true">
                        <label for="updateLevel"><b>Level</b></label>
                        <input type="text" name="updateLevel" value="${item.level}" required="true">
                        <input type="hidden" name="hardSkillId" value="${item.id}">
                        <div class="save-delete-btns">
                            <div class="save-btn">
                                <button type="submit" name="hardSubmit" value="update"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                            </div>
                            <div class="delete-btn">
                                <button type="submit" name="hardSubmit" value="delete"><img src="<c:url value="resources/images/icons/delete_icon.png"/>" alt="delete_icon"></button>
                            </div>
                        </div>
                    </form>
                </c:forEach>
                <c:forEach items="${softSkills}" var="item">
                    <form action="editprofile?query=updateSoftSkills" method="post">
                        <label for="updateName"><b>Soft Skill Name</b></label>
                        <input type="text" name="updateName" value="${item.name}" required="true">
                        <label for="updateLevel"><b>Level</b></label>
                        <input type="text" name="updateLevel" value="${item.level}" required="true">
                        <input type="hidden" name="softSkillId" value="${item.id}">
                        <div class="save-delete-btns">
                            <div class="save-btn">
                                <button type="submit" name="softSubmit" value="update"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                            </div>
                            <div class="delete-btn">
                                <button type="submit" name="softSubmit" value="delete"><img src="<c:url value="resources/images/icons/delete_icon.png"/>" alt="delete_icon"></button>
                            </div>
                        </div>
                    </form>
                </c:forEach>
                <c:if test="${true == sessionScope.invalidHardSkillLevel}">
                    <h5 class="error-msg">Please enter number to the level field!</h5>
                </c:if>

            </div>
            <!--END OF HIDDEN EDIT SKILL CONTAINER-->

            <div id="lang-container" class="prof-lang">
                <div class="edit-languages">
                    <button onclick="editLanguages()" class="edit-lang"><img src="<c:url value="resources/images/icons/edit_icon.png"/>"></button>
                </div>
                <div class="lang-header"><img src="<c:url value="resources/images/icons/language_icon.png"/>" alt="language_icon"><h3>Languages</h3></div>
                    <c:forEach items="${languages}" var="item">
                    <div class="lang-unit">
                        <p class="lang-name">${item.name}</p>
                        <p class="lang-lvl">Level: <span class="lang-level">${item.level}</span></p>
                        <div class="lang-lvl-bar"><h5 class="current-lang-level"></h5></div>
                    </div>
                </c:forEach>
                <div id="add-lang-btn" class="add-lang">
                    <button onclick="addLanguage()" ><img src="<c:url value="resources/images/icons/add_icon.png"/>"></button>
                </div>
                <!--HIDDEN ADD LANGUAGES CONTAINER-->
                <div id="add-language-container">
                    <form action="editprofile?query=addLanguage" method="post">
                        <label for="newName"><b>Language</b></label>
                        <input type="text" placeholder="Enter language" name="newName" required="true">
                        <label for="newLevel"><b>Level</b></label>
                        <select name="newLevel">
                            <option value="A1">A1</option>
                            <option value="A2">A2</option>
                            <option value="B1">B1</option>
                            <option value="B2">B2</option>
                            <option value="C1">C1</option>
                            <option value="C2">C2</option>
                        </select>
                        <div class="add-btn">
                            <button type="submit"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                        </div>
                        <br>
                    </form>
                </div>
                <!--end of add language container-->
            </div>

            <!--HIDDEN EDIT LANGUAGE CONTAINER-->
            <div id="edit-languages-container">
                <div class="lang-header"><img src="<c:url value="resources/images/icons/language_icon.png"/>" alt="language_icon"><h3>Languages</h3></div>
                    <c:forEach items="${languages}" var="item">
                    <form action="editprofile?query=updateLanguages" method="post">
                        <label for="updateName"><b>Language</b></label>
                        <input type="text" name="updateName" value="${item.name}" required="true">
                        <label for="updateLevel"><b>Level</b></label>
                        <select name="updateLevel">
                            <option value="${item.level}" selected hidden>${item.level}</option>
                            <option value="A1">A1</option>
                            <option value="A2">A2</option>
                            <option value="B1">B1</option>
                            <option value="B2">B2</option>
                            <option value="C1">C1</option>
                            <option value="C2">C2</option>
                        </select>
                        <input type="hidden" name="languageId" value="${item.id}">
                        <div class="save-delete-btns">
                            <div class="save-btn">
                                <button type="submit" name="languageSubmit" value="update"><img src="<c:url value="resources/images/icons/save_icon.png"/>"></button>
                            </div>
                            <div class="delete-btn">
                                <button type="submit" name="languageSubmit" value="delete"><img src="<c:url value="resources/images/icons/delete_icon.png"/>" alt="delete_icon"></button>
                            </div> 
                        </div>
                        <br>
                    </form>
                </c:forEach>
            </div>
            <!-- end of hidden language container-->

        </div>
        <script type="text/javascript" src="<c:url value="resources/js/profile.js"/>"></script>
        <script type="text/javascript" src="<c:url value="resources/js/editprofile.js"/>"></script>
    </body>
</html>
